#!/usr/bin/ruby
# encoding: utf-8
require 'fileutils'
require 'rasem'
# https://github.com/aseldawy/rasem

imgname = File.basename(__FILE__,".*")
fnts = 40

img = Rasem::SVGImage.new(:width => 150, :height => 150) do
  defs {
    group(:id => "arrow") {
      polygon([[0,0],[32,-10],[32,10]],:stroke=>"none",:fill=>"#000000")
      line(32,0,50,0,:stroke=>"#000000",:stroke_width=>4)
      }
    }

  rectangle(0,0,150,150,:stroke=>"#000000",:fill=>"#ffffff", :stroke_width=>4) # background     
  use("arrow").translate(75,20).rotate(90)

  text(75,125,"text-anchor"=>"middle", "dominant-baseline"=>"auto","font-family"=>"sans-serif","font-size"=>fnts,:fill=>"#000000"){raw imgname}
  end

  FileUtils.mkpath("site/images")
  file = "site/images/" + imgname + ".svg"

  File.open(file, "w") { |f|
    img.write(f)
    f.write("<!-- Norbert.Reschke@gMail.com, https://creativecommons.org/licenses/by-nc-sa/4.0 -->")
    f.write("\n")
    f.write("<!-- made with rasem: https://github.com/aseldawy/rasem -->")
    }

