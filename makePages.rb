#!/usr/bin/ruby
# encoding: utf-8
require 'fileutils'
require 'date'
require 'asciidoctor'
require 'git'
require_relative 'makeImage'

# unbedingt beachten und dieses script auf Gültigkeit prüfen
# https://bass.schul-welt.de/5667.htm # Kapitel 15 Gültigkeitsliste
# https://bass.schul-welt.de/3129.htm # 13-33 Nr.1.1 Verordnung über die Ausbildung und Prüfung in den Bildungsgängen des Berufskollegs
# https://bass.schul-welt.de/3787.htm # s.o. ?
# https://bass.schul-welt.de/11020.htm # 12-21 Nr.1 Berufliche Orientierung
# https://bass.schul-welt.de/9133.htm # Neue und geänderte Vorschriften seit der Vorjahres-BASS
# https://www.berufsbildung.nrw.de/cms/bildungsgaenge-bildungsplaene/uebersicht/index.html

FileUtils.mkpath("tmp")
FileUtils.cp("adoc.css","site/adoc.css")

# naming some things...
# Fachbereich Technik/Naturwissenschaften, https://www.berufsbildung.nrw.de/cms/bildungsgaenge-bildungsplaene/ausbildungsvorbereitung-anlage-a/bildungsplaene/fachbereich-technik-naturwissenschaften.html
bfbh = "AVTBH" # (1) Berufsfeld Bau- und Holztechnik, https://www.berufsbildung.nrw.de/cms/upload/ausbildungsvorbereitung/technik_naturwissenschaft/av_technik_bau-holztechnik.pdf
bfet = 'AVTET' # (2) Berufsfeld Elektrotechnik, https://www.berufsbildung.nrw.de/cms/upload/ausbildungsvorbereitung/technik_naturwissenschaft/av_technik_elektrotechnik.pdf
bfkf = 'AVTKF' # (3) Berufsfeld Fahrzeugtechnik, https://www.berufsbildung.nrw.de/cms/upload/ausbildungsvorbereitung/technik_naturwissenschaft/av_technik_fahrzeugtechnik.pdf
bfmt = 'AVTMT' # (4) Berufsfeld Metalltechnik, https://www.berufsbildung.nrw.de/cms/upload/ausbildungsvorbereitung/technik_naturwissenschaft/av_technik_metalltechnik.pdf
av1 = 'APO-BK-AA-2.1'
av2 = 'APO-BK-AA-2.2'
av3 = 'APO-BK-AAVV-p22'
idx = "tmp/index.asciidoc"
wpt = "week-part-time"
wft = "week-full-time"
wif = "week-ifk"
asbh11 = "AV-BH-AS-1.1"
asbh12 = "AV-BH-AS-1.2"
asbh21 = "AV-BH-AS-2.1"
asbh22 = "AV-BH-AS-2.2"
asbh23 = "AV-BH-AS-2.3"
asbh24 = "AV-BH-AS-2.4"
asbh31 = "AV-BH-AS-3.1"
asbh32 = "AV-BH-AS-3.2"
asbh41 = "AV-BH-AS-4.1"
asbh42 = "AV-BH-AS-4.2"
asbh51 = "AV-BH-AS-5.1"
asbh61 = "AV-BH-AS-6.1"
asma1 = "AV-Ma-AS-1"
asma2 = "AV-Ma-AS-2"
asma3 = "AV-Ma-AS-3"
asma4 = "AV-Ma-AS-4"
asma5 = "AV-Ma-AS-5"
asen1 = "AV-En-AS-1"
asen2 = "AV-En-AS-2"
asen3 = "AV-En-AS-3"
asen4 = "AV-En-AS-4"
asen5 = "AV-En-AS-5"
asen6 = "AV-En-AS-6"
asde1 = "AV-De-AS-1"
asde2 = "AV-De-AS-2"
asde3 = "AV-De-AS-3"
asde4 = "AV-De-AS-4"
asde5 = "AV-De-AS-5"
aswi1 = "AV-Wi-AS-1"
aswi2 = "AV-Wi-AS-2"
aswi3 = "AV-Wi-AS-3"
assp1 = "AV-Sp-AS-1"
assp2 = "AV-Sp-AS-2"
assp3 = "AV-Sp-AS-3"
assp4 = "AV-Sp-AS-4"
assp5 = "AV-Sp-AS-5"
assp6 = "AV-Sp-AS-6"
aspo1 = "AV-Po-AS-1"
aspo2 = "AV-Po-AS-2"
aspo3 = "AV-Po-AS-3"
aspo4 = "AV-Po-AS-4"
asre1 = "AV-Re-AS-1"
asre2 = "AV-Re-AS-2"
asre3 = "AV-Re-AS-3"
asre4 = "AV-Re-AS-4"
asre5 = "AV-Re-AS-5"
asre6 = "AV-Re-AS-6"

# hours for subjects
_Ma = 2 # Mathe
_En = 2 # Englisch
_Wi = 1 # Wirtschaftslehre
_Na = 0 # Naturwissenschaft
_De = 2 # Deutsch
_Re = 1 # Religion
_Sp = 1 # Sport
_Po = 1 # Politik
_Fu = 1 # Differenzierung = Förderunterricht
  
# links to education plans
_l_BO = "link:https://www.berufsbildung.nrw.de/cms/upload/ausbildungsvorbereitung/technik_naturwissenschaft/av_technik_bau-holztechnik.pdf[Betriebs&shy;organisation]"
_l_PE = "link:https://www.berufsbildung.nrw.de/cms/upload/ausbildungsvorbereitung/technik_naturwissenschaft/av_technik_bau-holztechnik.pdf[Produkt&shy;erstellung]"
_l_Ma = "link:https://www.berufsbildung.nrw.de/cms/upload/ausbildungsvorbereitung/technik_naturwissenschaft/av_technik_mathe.pdf[Mathe&shy;matik <<note2,^2)^>>]"
_l_En = "link:https://www.berufsbildung.nrw.de/cms/upload/ausbildungsvorbereitung/technik_naturwissenschaft/av_technik_englisch.pdf[Englisch <<note2,^2,^>> <<note3,^3)^>>]"
_l_Na = "link:https://www.berufsbildung.nrw.de/cms/upload/ausbildungsvorbereitung/technik_naturwissenschaft/av_technik_naturwissenschaft.pdf[Natur&shy;wissen&shy;schaft]"
_l_Wi = "link:https://www.berufsbildung.nrw.de/cms/upload/ausbildungsvorbereitung/technik_naturwissenschaft/av_technik_wbl.pdf[Wirt&shy;schafts- und Betriebs&shy;lehre]"
_l_De = "link:https://www.berufsbildung.nrw.de/cms/upload/ausbildungsvorbereitung/technik_naturwissenschaft/av_technik_deutsch.pdf[Deutsch/&shy;Kom&shy;mu&shy;nikation]"
_l_Re = "Religionslehre <<note4,^4)^>> <<note5,^5)^>> <<note6,^6)^>> <<note7,^7)^>>"
#_l_Re = "Religionslehre <<note4,^4)^>> <<note5,^5)^>> <<note6,^6)^>> <<note7,^7)^>>  link:https://www.berufsbildung.nrw.de/cms/upload/ausbildungsvorbereitung/technik_naturwissenschaft/av_technik_kath-rel.pdf[KR], link:https://www.berufsbildung.nrw.de/cms/upload/ausbildungsvorbereitung/technik_naturwissenschaft/av_technik_ev-rel.pdf[ER], link:https://www.berufsbildung.nrw.de/cms/upload/ausbildungsvorbereitung/technik_naturwissenschaft/av_technik_islamische-religion.pdf[IL]"
# Religionslehre Kürzel aus Schlüsseltabellen Schild-NRW
# https://schulverwaltungsprogramme.msb.nrw.de/download/Berufskolleg.pdf
_l_Sp = "link:https://www.berufsbildung.nrw.de/cms/upload/ausbildungsvorbereitung/technik_naturwissenschaft/av_technik_sport.pdf[Sport/&shy;Gesund&shy;heits&shy;förderung]"
_l_Po = "link:https://www.berufsbildung.nrw.de/cms/upload/ausbildungsvorbereitung/technik_naturwissenschaft/av_technik_politik.pdf[Politik/&shy;Gesell&shy;schafts&shy;lehre]"

#links to Anforderungssituationen
_BHAS11 = "link:" + asbh11 + "{ext-relative}[AS-1.1]"
_BHAS12 = "link:" + asbh12 + "{ext-relative}[AS-1.2]"
_BHAS21 = "link:" + asbh21 + "{ext-relative}[AS-2.1]"
_BHAS22 = "link:" + asbh22 + "{ext-relative}[AS-2.2]"
_BHAS23 = "link:" + asbh23 + "{ext-relative}[AS-2.3]"
_BHAS24 = "link:" + asbh24 + "{ext-relative}[AS-2.4]"
_BHAS31 = "link:" + asbh31 + "{ext-relative}[AS-3.1]"
_BHAS32 = "link:" + asbh32 + "{ext-relative}[AS-3.2]"
_BHAS41 = "link:" + asbh41 + "{ext-relative}[AS-4.1]"
_BHAS42 = "link:" + asbh42 + "{ext-relative}[AS-4.2]"
_BHAS51 = "link:" + asbh51 + "{ext-relative}[AS-5.1]"
_BHAS61 = "link:" + asbh61 + "{ext-relative}[AS-6.1]"

_MaAS1 = "link:" + asma1 + "{ext-relative}[AS-1]"
_MaAS2 = "link:" + asma2 + "{ext-relative}[AS-2]"
_MaAS3 = "link:" + asma3 + "{ext-relative}[AS-3]"
_MaAS4 = "link:" + asma4 + "{ext-relative}[AS-4]"
_MaAS5 = "link:" + asma5 + "{ext-relative}[AS-5]"

_EnAS1 = "link:" + asen1 + "{ext-relative}[AS-1]"
_EnAS2 = "link:" + asen2 + "{ext-relative}[AS-2]"
_EnAS3 = "link:" + asen3 + "{ext-relative}[AS-3]"
_EnAS4 = "link:" + asen4 + "{ext-relative}[AS-4]"
_EnAS5 = "link:" + asen5 + "{ext-relative}[AS-5]"
_EnAS6 = "link:" + asen6 + "{ext-relative}[AS-6]"

_WiAS1 = "link:" + aswi1 + "{ext-relative}[AS-1]"
_WiAS2 = "link:" + aswi2 + "{ext-relative}[AS-2]"
_WiAS3 = "link:" + aswi3 + "{ext-relative}[AS-3]"

_DeAS1 = "link:" + asde1 + "{ext-relative}[AS-1]" 
_DeAS2 = "link:" + asde2 + "{ext-relative}[AS-2]" 
_DeAS3 = "link:" + asde3 + "{ext-relative}[AS-3]" 
_DeAS4 = "link:" + asde4 + "{ext-relative}[AS-4]" 
_DeAS5 = "link:" + asde5 + "{ext-relative}[AS-5]" 

_ReAS1 = "link:" + asre1 + "{ext-relative}[AS-1]"
_ReAS2 = "link:" + asre2 + "{ext-relative}[AS-2]"
_ReAS3 = "link:" + asre3 + "{ext-relative}[AS-3]"
_ReAS4 = "link:" + asre4 + "{ext-relative}[AS-4]"
_ReAS5 = "link:" + asre5 + "{ext-relative}[AS-5]"
_ReAS6 = "link:" + asre6 + "{ext-relative}[AS-6]"

_SpAS1 = "link:" + assp1 + "{ext-relative}[AS-1]"
_SpAS2 = "link:" + assp2 + "{ext-relative}[AS-2]"
_SpAS3 = "link:" + assp3 + "{ext-relative}[AS-3]"
_SpAS4 = "link:" + assp4 + "{ext-relative}[AS-4]"
_SpAS5 = "link:" + assp5 + "{ext-relative}[AS-5]"
_SpAS6 = "link:" + assp6 + "{ext-relative}[AS-6]"

_PoAS1 = "link:" + aspo1 + "{ext-relative}[AS-1]"
_PoAS2 = "link:" + aspo2 + "{ext-relative}[AS-2]"
_PoAS3 = "link:" + aspo3 + "{ext-relative}[AS-3]"
_PoAS4 = "link:" + aspo4 + "{ext-relative}[AS-4]"

# weekplan, matrixplan, look at bottom
_BOAS = "Betriebsorganisation"           # Betriebsorganisation, Anforderungssituation
_PEAS = "Produkterstellung"              # Produkterstellung, Anforderungssituation
_MaAS = "Mathematik"                     # Mathe, Anforderungssituation
_EnAS = "Englisch"                       # Englisch, Anforderungssituation
_WiAS = "Wirtschafts- und Betriebslehre" # Wirtschaftslehre, Anforderungssituation
_NaAS = "Naturwissenschaft"              # Naturwissenschaft, Anforderungssituation
_DeAS = "Deutsch/Kommunikation"          # Deutsch, Anforderungssituation    
_ReAS = "Religionslehre"                 # Religion, Anforderungssituation
_SpAS = "Sport/Gesundheitsförderung"     # Sport, Anforderungssituation
_PoAS = "Politik/Gesellschaftslehre"     # Politik, Anforderungssituation

array_week_AS = [["begin subjects",_BOAS,_PEAS,_MaAS,_EnAS,_WiAS,_NaAS,_DeAS,_ReAS,_SpAS,_PoAS,"end subjects"]]
array_week_LS = [[]]
(0..11).each { |i|
  array_week_LS.push(array_week_AS[0][i])
}

git = Git.open('.')

# begin making tmp/index.txt and render site/index.html
f = File.new(idx, "w")
f.write(":ext-relative: {outfilesuffix} \n")
f.write(":notitle:\n")
f.write(":nofooter:\n")
f.write(":doctype: article \n")
f.write(":doctitle: Jahresplanung Ausbildungsvorbereitung \n")
f.write(":authors: Norbert Reschke \n")
f.write(":subject: Didaktische Jahresplanung \n")
f.write(":keywords: Didaktische Jahresplanung, Ausbildungsvorbereitung, Nordrhein-Westfalen, Fachbereich Technik, Berufsfeld Bau- und Holztechnik \n")
f.write(":copyright: CC-BY-SA 4.0 \n")
f.write(":email: Norbert.Reschke@gMail.com \n")
f.write("=== Jahresplanung Ausbildungsvorbereitung \n")
(1..40).each { |i|
  f.write("[.flex03] \n")
  f.write("image::"+ "%02d" % i.to_s + ".svg[Ausbildungsvorbereitung Woche " + "%02d" % i.to_s + ",150px,150px,link=" + bfbh + "-" + wpt +"-%02d" % i.to_s + "{outfilesuffix}] \n")
}
(1..4).each { |i|
  f.write("[.flex03] \n")
  f.write("image::fake.svg[fake image to use fixed buttons] \n")
} # ugly workaround to get rid of space-around in last row, there may be much better solutions
f.write("\n")
#f.write("[.flex04] \n")
f.write("Grundlagen bilden die APO-BK Anlage A und die Bildungspläne für die Ausbildungsvorbereitung. \n") 
f.write("Diese Seiten berücksichtigen bis jetzt die Ausbildungsvorbereitung im Fachbereich Technik/Naturwissenschaften \n")
f.write("und das Berufsfeld Bautechnik und Holztechnik. \n")
f.write(" \n")
f.write("[.flex01] \n")
f.write("Zu den Grundlagen: \n")
f.write("[.flex02] \n")
f.write("link:" + av1 + "{ext-relative}[APO-BK Anlage A 2.1] +\n")
f.write("link:" + av2 + "{ext-relative}[APO-BK Anlage A 2.2] +\n")
f.write("link:" + av3 + "{ext-relative}[APO-BK Anlage A § 22] +\n")
f.write(" \n")
f.write("image:fake.svg[fake image to use fixed buttons] +\n")
f.write(" \n")
f.write("[l1b]#image:ccav.svg[license,100%,link=license{ext-relative}]# \n")
f.write("[l2b]#image:matrix.svg[matrix,100%,link=matrix-02-01{ext-relative}]# \n")
f.close

Asciidoctor.render_file idx,
:base_dir => '.',
:to_dir => 'site/',
:safe => 'safe',
:attributes => 'linkcss stylesdir=. stylesheet=adoc.css imagesdir=images lang=de'
# end making tmp/index.asciidoc and render site/index.html

# begin making tmp/Anlage A2.*.asciidoc and render site/APO-BK-Anlage-A2.*.html
pbk = {'https://bass.schul-welt.de/3129.htm#13-33nr3.1AnlageA2.1':av1,
       'https://bass.schul-welt.de/3129.htm#13-33nr3.1AnlageA2.2':av2,
       'https://bass.schul-welt.de/Stichwort/3129.htm#13-33nr1.1p22_AnlageA':av3
      }
# zu VVp22 siehe auch:
# https://bass.schul-welt.de/16778.htm # 13-63 Nr.4, Besondere Bestimmungen für den Unterricht für geflüchtete Jugendliche im Alter von 16 bis 25 Jahren Schülerinnen und Schüler in Klassen des Berufskollegs ("Fit für Mehr")
# https://bass.schul-welt.de/18425.htm # 13-63 Nr.3, Integration und Deutschförderung neu zugewanderter Schülerinnen und Schüler

pbk.each {|i,j|
  case j
    when av1
      frag = wpt
    when av2
      frag = wft
    when av3
      frag = wif
  end
  doc = "tmp/" + j + ".asciidoc"
  f = File.new(doc, "w")
  f.write(":ext-relative: {outfilesuffix} \n")
  f.write(":nofooter:\n")
  f.write(":doctype: article \n")
  f.write(":doctitle: " + j + " \n")
  f.write(":authors: Norbert Reschke \n")
  f.write(":subject: " + j + " \n")
  f.write(":keywords: APO-BK, Didaktische Jahresplanung, Ausbildungsvorbereitung, Nordrhein-Westfalen, Fachbereich Technik, Berufsfeld Bau- und Holztechnik \n")
  f.write(":copyright: CC-BY-SA 4.0 \n")
  f.write(":email: Norbert.Reschke@gMail.com \n")
  f.write(" \n")
  f.write("link:" + i.to_s + "[" + j + "] \n")
  f.write(" und Ergänzungen aus dem Bildungsplan Ausbildungsvorbereitung, \n")
  f.write("Fachbereich Technik/Naturwissenschaften, Berufsfeld Bau- und Holztechnik, \n")
  f.write("bereichsspezifische Fächer: Betriebsorganisation , Produkterstellung, MSW-Nordrhein-Westfalen, 42031/2015 \n")
  f.write(" \n")
  f.write("Der entsprechende \n")
  f.write("link:" + frag + "{ext-relative}[Wochenplan] \n")
  f.write("wird aus diesem Bandbreitenmodell entwickelt. \n")
  f.write("include::" + j + ".txt[] \n")
  f.write(" \n")
  f.write("image:fake.svg[fake image to use fixed buttons] +\n")
  f.write(" \n")
  f.write("[l1b]#image:home.svg[home,100%, link=index{ext-relative}]# \n")
  f.write("[l2b]#image:matrix.svg[matrix,100%,link=matrix-02-01{ext-relative}]# \n")
  f.write("[bottomrightmark]#Ver: " + git.log(1).path( + j + ".txt").to_s[0..7] + "# \n")
  f.close
  Asciidoctor.render_file doc,
  :base_dir => '.',
  :to_dir => 'site/',
  :safe => 'safe',
  :attributes => 'linkcss stylesdir=. stylesheet=adoc.css imagesdir=images lang=de'

# begin making tmp/week-*-time.asciidoc and render site/week-*-time.html
  doc = "tmp/" + frag + ".asciidoc"
  f = File.new(doc, "w")
  f.write(":ext-relative: {outfilesuffix} \n")
  f.write ":notitle:\n"
  f.write(":nofooter:\n")
  f.write(":doctype: article \n")
  f.write(":doctitle: " + frag + " \n")
  f.write(":authors: Norbert Reschke \n")
  f.write(":subject: " + frag + " \n")
  f.write(":keywords: Wochenplan, APO-BK, Didaktische Jahresplanung, Ausbildungsvorbereitung, Nordrhein-Westfalen, Fachbereich Technik, Berufsfeld Bau- und Holztechnik \n")
  f.write(":copyright: CC-BY-SA 4.0 \n")
  f.write(":email: Norbert.Reschke@gMail.com \n")
  f.write(" \n")
  f.write("Wochenplan nach link:" + i.to_s + "{outfilesuffix}[" + j + "] \n")
  f.write(" \n")
  f.write("include::" + frag + ".txt[] \n")
  f.write( " \n")
  f.write("image:fake.svg[fake image to use fixed buttons] +\n")
  f.write(" \n")
  f.write("[l1b]#image:home.svg[home,100%,link=index{ext-relative}]# \n")
  f.write("[l2b]#image:matrix.svg[matrix,100%,link=matrix-02-01{ext-relative}]# \n")
  f.write("[bottomrightmark]#Ver: " + git.log(1).path( + frag + ".txt").to_s[0..7] + "# \n")
  f.close
  Asciidoctor.render_file doc,
  :base_dir => '.',
  :to_dir => 'site/',
  :safe => 'safe',
  :attributes => 'linkcss stylesdir=. stylesheet=adoc.css imagesdir=images lang=de'
# end making tmp/week-*-time.asciidoc and render site/week-*-time.html
# end making tmp/Anlage A2.*.asciidoc and render site/APO-BK-Anlage-A2.*.html
}

# begin making tmp/*AS*.asciidoc and render site/*AS*-time.html
arr=[
asbh11,asbh12,asbh21,asbh22,asbh23,asbh24,asbh31,asbh32,asbh41,asbh42,asbh51,asbh61,
asma1,asma2,asma3,asma4,asma5,
asen1,asen2,asen3,asen4,asen5,asen6,
asde1,asde2,asde3,asde4,asde5,
aswi1,aswi2,aswi3,
assp1,assp2,assp3,assp4,assp5,assp6,
aspo1,aspo2,aspo3,aspo4,
asre1,asre2,asre3,asre4,asre5,asre6
]

arr.each { |b| 
  case b[3..4].downcase
    when "bh"
      subject = "02"
    when "ma"
      subject = "03"
    when "en"
      subject = "04"
    when "wi"
      subject = "05"
    when "de"
      subject = "07"
    when "re"
      subject = "08"
    when "sp"
      subject = "09"
    when "po"
      subject = "10"
  end
  as = "tmp/" + b + ".asciidoc"
  f = File.new(as, "w")
  f.write(":ext-relative: {outfilesuffix} \n")
  f.write ":notitle:\n"
  f.write(":nofooter:\n")
  f.write(":doctype: article \n")
  f.write(":doctitle: " + b + " \n")
  f.write(":authors: Norbert Reschke \n")
  f.write(":subject: " + b + " \n")
  f.write(":keywords: Anforderungssituation, Ausbildungsvorbereitung, Nordrhein-Westfalen, Fachbereich Technik, Berufsfeld Bau- und Holztechnik \n")
  f.write(":copyright: CC-BY-SA 4.0 \n")
  f.write(":email: Norbert.Reschke@gMail.com \n")
  f.write(" \n")
# begin get image and link in and to LS-pages
  if b[3..4] != "BH"   # get name of subject (like it is used in array_week_AS, example _MaAS = "Mathematik")
    c = eval("_" + b[3..4] + b[6..7])
  else
    c = "Technologie"   # workaround, because BH could be Betriebsorganisation or Produktgestaltung
  end

  if b[11] == nil
    imgnmbr = b[9]         # example: b = "AV-Ma-AS-1"
  elsif b[11] != nil
    imgnmbr = b[9] + b[11] # example: b = "AV-BH-AS-6.1"
  end
  f.write("image::" + b[6..7].upcase + b[3..4].upcase + imgnmbr + ".svg")
  f.write("[ " + c + " Anforderungssituation " + b[9..11] + "] \n")
  f.write("include::" + b + ".txt[] \n")
  f.write(" \n")
  f.write("image:fake.svg[fake image to use fixed buttons] +\n")
  f.write("[l1b]#image:home.svg[home,100%, link=index{ext-relative}]# \n")
  f.write("[l2b]#image:matrix.svg[Matrix 40 Wochen,100%, link=matrix-" + subject + "-01{ext-relative}]# \n")
  f.write("[l3b]#image:LS" + b[6..7].upcase + b[3..4].upcase + imgnmbr + ".svg")
  f.write("[ " + c + " Lernsituation " + b[9..11] + ",100%, link=LS-" + b + "{outfilesuffix}]# \n")
  f.write("[bottomrightmark]#Ver: " + git.log(1).path( + b + ".txt").to_s[0..7] + "# \n")
  f.close

  Asciidoctor.render_file as,
  :base_dir => '.',
  :to_dir => 'site/',
  :safe => 'safe',
  :attributes => 'linkcss stylesdir=. stylesheet=adoc.css imagesdir=images lang=de'

# end making tmp/*AS*.asciidoc and render site/*AS*-time.html

# begin making tmp/LS*.asciidoc and render site/LS*-time.html
  ls = "tmp/LS-" + b + ".asciidoc"
  f = File.new(ls, "w")
  f.write(":ext-relative: {outfilesuffix} \n")
  f.write ":notitle:\n"
  f.write(":nofooter:\n")
  f.write(":doctype: article \n")
  f.write(":doctitle: LS-" + b + " \n")
#  f.write(":authors: Norbert Reschke \n")
#  this must be rewritten, because authors can also participate with E-Mail-fragments
#  workaround follows
  case b[3..4].downcase
#    when "bh"
#    when "ma"
#    when "en"
#    when "wi"
#    when "de"
#    when "re"
    when "sp"
      f.write(":authors: Julia Schumacher \n")
      f.write(":description: Lernsituationen der Fachkonferenz Sport am Hans-Sachs-Berufskolleg, Oberhausen \n")
#    when "po"
    else
      f.write(":authors: Bildungsgangkonferenz Ausbildungsvorbereitung \n")
      f.write(":description: Lernsituationen in der Ausbildungsvorbereitung am Hans-Sachs-Berufskolleg, Oberhausen \n")
  end
  f.write(":subject: LS-" + b + " \n")
  f.write(":keywords: Lernsituation, Ausbildungsvorbereitung, Nordrhein-Westfalen, Fachbereich Technik, Berufsfeld Bau- und Holztechnik \n")
  f.write(":copyright: CC-BY-SA 4.0 \n")
  f.write(":email: Norbert.Reschke@gMail.com \n")
  f.write(" \n")
  f.write("[l1t]#image:LS" + b[6..7].upcase + b[3..4].upcase + imgnmbr + ".svg")
  f.write("[ " + c + " Lernsituation " + b[9..11] + ",100%]# \n")
  f.write("\n")
  f.write("[.a]\n")
  f.write("Schule\n")
  f.write("\n")
  f.write("[.b]\n")
  f.write("Hans Sachs Berufskolleg in Oberhausen \n")
  f.write("\n")
  f.write("[.a]\n")
  f.write("Fachbereich\n")
  f.write("\n")
  f.write("[.b]\n")
  f.write("Technik/Naturwissenschaften\n")
  f.write("\n")
  f.write("[.a]\n")
  f.write("Berufsfeld\n")
  f.write("\n")
  f.write("[.b]\n")
  f.write("Bau- und Holztechnik\n")
  f.write("\n")
  f.write("[.a]\n")
  f.write("Bildungsgang\n")
  f.write("[.b]\n")
  f.write("Ausbildungsvorbereitung für Schülerinnen und Schüler ohne Berufsausbildungsverhältnis (APO-BK Anlage{nbsp}A)\n")
  f.write("\n")
  f.write("[.a]\n")
  f.write("Fach\n")
  f.write("\n")
  f.write("[.b]\n")
  if c == "Technologie"
    c = "Produkterstellung¹"
    d = "¹ alle Inhalte der Betriebsorganisation werden im Fach Produkterstellung vermittelt. Das Fach Betriebsorganisation entfällt."
  else
    d = ""
  end
  f.write( c +"\n")
  f.write("\n")
  f.write("[.a]\n")
  f.write("* Handlungsfeld\n")
  f.write("* Lernfeld\n")
  f.write("\n")
  f.write("include::LS-" + b + ".txt[] \n")
  f.write("\n")
  f.write( d + "\n")
  f.write("\n")
  f.write("{authors}, {description} \n")
  f.write(" \n")
  f.write("image:fake.svg[fake image to use fixed buttons,1px,100px] +\n")
  f.write("[l1b]#image:home.svg[home,100%, link=index{ext-relative}]# \n")
  f.write("[l2b]#image:matrix.svg[Matrix 40 Wochen,100%, link=matrix-" + subject + "-01{ext-relative}]# \n")
  f.write("[l3b]#image:" + b[6..7].upcase + b[3..4].upcase + imgnmbr + ".svg")
  f.write("[ " + c + " Anforderungssituation " + b[9..11] + ",100%, link=" + b + "{outfilesuffix}]# \n")
  f.write("[bottomrightmark]#Ver: ")
# use next line if you only want to show hash of last commit (it's like this on command-line: git log -n1 --pretty=format:%h LS*1.1*) 
# f.write(git.log(1).path( + "LS-" + b + ".txt").to_s[0..6] )
# use next line if you want to link to the last commit of this file on bitbucket.org (it's like this on command-line: git log -n1 -p --pretty=format:%h LS*1.1*) 
  f.write("https://bitbucket.org/mawima/avtbh/commits/" + git.log(1).path( + "LS-" + b + ".txt").to_s + "[" + git.log(1).path( + "LS-" + b + ".txt").to_s[0..6] + "]")
  f.write(" - ")
# use next line if you want to link to all commits of this file on bitbucket.org (it's nearly like this on command-line: git log -p --pretty=format:%h LS*1.1*)
  f.write("(https://bitbucket.org/mawima/avtbh/commits/all?search=LS-" + b + ".txt[all{nbsp}changes])" )
  f.write("# \n")
  f.close

  Asciidoctor.render_file ls,
  :base_dir => '.',
  :to_dir => 'site/',
  :safe => 'safe',
  :attributes => 'linkcss stylesdir=. stylesheet=adoc.css imagesdir=images lang=de'
}
# end making tmp/LS*.asciidoc and render site/LS*-time.html

# begin making week pages
for m in 1..40
  if m == 40
    m_next = 1
  elsif m < 40
    m_next = m + 1
  end
  if m == 1
    m_back = 40
  elsif m > 1
    m_back = m - 1
  end
  thisPage = bfbh + "-" + wpt + "-%02d" % m.to_s
  nextPage = bfbh + "-" + wpt + "-%02d" % m_next.to_s
  backPage = bfbh + "-" + wpt + "-%02d" % m_back.to_s


# again naming some things, this is for the weekplan
  _BeBe = 8 # Berufsbezogener Lernbereich
  _BeAl = 5 # Berufsübergreifender Lernbereich (Allgemeiner Lernbereich)
  _BeDi = 1 # Lernbereich der Differenzierung
  _FaFa = 3 # Fächer des Fachbereichs

  s_BO = 0
  s_PE = 0

  subjects = case m
    when 1..18
      _BO = 0
      _PE = 3
      sBO = _BO + sBO.to_i
      sPE = _PE + sPE.to_i
    when 19..23
      _BO = 3
      _PE = 0
      sBO = _BO + sBO.to_i
      sPE = _PE + sPE.to_i
    when 24..37
      _BO = 0
      _PE = 3
      sBO = _BO + sBO.to_i
      sPE = _PE + sPE.to_i
    when 37..40
      _BO = 3
      _PE = 0
      sBO = _BO + sBO.to_i
      sPE = _PE + sPE.to_i
    else
      _BO = 0
      _PE = 0
  end

  _SuSu = _BO+_PE+_Ma+_En+_Wi+_Na+_De+_Re+_Sp+_Po+_Fu # Summe der Stunden in einer (dieser) Woche
  _SuWo = (_Ma+_En+_Wi+_Na+_De+_Re+_Sp+_Po+_Fu)*m + sBO + sPE # Summe der Stunden aller Wochen bis zu einschließlich dieser Woche

# Anforderungssituationen im Wochenplan und im Matrixplan
# look at array_week_AS on top -> naming some things...

  situations = case m  # Woche 1-40
    when 1
      i=1
      _BOAS = "-"                   # Betriebsorganisation, Anforderungssituation
      _PEAS = _BHAS21 + " (6/3)"    # Produkterstellung, Anforderungssituation
      _MaAS = _MaAS1 + " (16/2)"    # Mathe, Anforderungssituation
      _EnAS = _EnAS3 + " (14/2)"    # Englisch, Anforderungssituation
      _WiAS = _WiAS2 + " (15/1)"    # Wirtschaftslehre, Anforderungssituation
      _NaAS = "-"                        # Naturwissenschaft, Anforderungssituation
      _DeAS = _DeAS2 + " (10/2)"    # Deutsch, Anforderungssituation
      _ReAS = _ReAS3 + " (7/1)"     # Religion, Anforderungssituation
      _SpAS = _SpAS1 + " (8/1)"     # Sport, Anforderungssituation
      _PoAS = _PoAS2 + " (8/1)"     # Politik, Anforderungssituation
      _BOLS = "-"                                                               # Betriebsorganisation, Anforderungssituation
      _PELS = "link:LS-" + asbh21 + "{ext-relative}[LS zu AS{nbsp}-{nbsp}2.1]"  # Produkterstellung, Lernsituation
      _MaLS = "link:LS-" + asma1 + "{ext-relative}[LS zu AS{nbsp}-{nbsp}1]"     # Mathe, Lernsituation
      _EnLS = "link:LS-" + asen3 + "{ext-relative}[LS zu AS{nbsp}-{nbsp}3]"     # Englisch, Lernsituation
      _WiLS = "link:LS-" + aswi2 + "{ext-relative}[LS zu AS{nbsp}-{nbsp}2]"     # Wirtschaftslehre, Lernsituation
      _NaLS = "-"                                                               # Naturwissenschaft, Lernsituation
      _DeLS = "link:LS-" + asde2 + "{ext-relative}[LS zu AS{nbsp}-{nbsp}2]"     # Deutsch, Lernsituation
      _ReLS = "link:LS-" + asre3 + "{ext-relative}[LS zu AS{nbsp}-{nbsp}3]"     # Religion, Lernsituation
      _SpLS = "link:LS-" + assp1 + "{ext-relative}[LS zu AS{nbsp}-{nbsp}1]"     # Sport, Lernsituation
      _PoLS = "link:LS-" + aspo2 + "{ext-relative}[LS zu AS{nbsp}-{nbsp}2]"     # Politik, Lernsituation
      array_week_AS[i] = ["begin AS in week " +i.to_s,_BOAS,_PEAS,_MaAS,_EnAS,_WiAS,_NaAS,_DeAS,_ReAS,_SpAS,_PoAS,"end AS in week " +i.to_s]
      array_week_LS[i] = ["begin LS in week " +i.to_s,_BOLS,_PELS,_MaLS,_EnLS,_WiLS,_NaLS,_DeLS,_ReLS,_SpLS,_PoLS,"end LS in week " +i.to_s]
 
    when 2
      i=i+1
      _BOAS = "-"
      _PEAS = _BHAS21 + " (6/6)"
      _MaAS = _MaAS1 + " (16/4)"
      _EnAS = _EnAS3 + " (14/4)"
      _WiAS = _WiAS2 + " (15/2)"
      _NaAS = "-"
      _DeAS = _DeAS2 + " (10/4)"
      _ReAS = _ReAS3 + " (7/2)"
      _SpAS = _SpAS1 + " (8/2)"
      _PoAS = _PoAS2 + " (8/2)"
      _BOLS = "-"
      _PELS = "link:LS-" + asbh21 + "{ext-relative}[LS zu AS{nbsp}-{nbsp}2.1]"
      _MaLS = "link:LS-" + asma1 + "{ext-relative}[LS zu AS{nbsp}-{nbsp}1]"
      _EnLS = "link:LS-" + asen3 + "{ext-relative}[LS zu AS{nbsp}-{nbsp}3]"
      _WiLS = "link:LS-" + aswi2 + "{ext-relative}[LS zu AS{nbsp}-{nbsp}2]"
      _NaLS = "-"
      _DeLS = "link:LS-" + asde2 + "{ext-relative}[LS zu AS{nbsp}-{nbsp}2]"
      _ReLS = "link:LS-" + asre3 + "{ext-relative}[LS zu AS{nbsp}-{nbsp}3]"
      _SpLS = "link:LS-" + assp1 + "{ext-relative}[LS zu AS{nbsp}-{nbsp}1]"
      _PoLS = "link:LS-" + aspo2 + "{ext-relative}[LS zu AS{nbsp}-{nbsp}2]"
      array_week_AS[i] = ["begin AS in week " +i.to_s,_BOAS,_PEAS,_MaAS,_EnAS,_WiAS,_NaAS,_DeAS,_ReAS,_SpAS,_PoAS,"end AS in week " +i.to_s]
      array_week_LS[i] = ["begin LS in week " +i.to_s,_BOLS,_PELS,_MaLS,_EnLS,_WiLS,_NaLS,_DeLS,_ReLS,_SpLS,_PoLS,"end LS in week " +i.to_s]

    when 3
      i=i+1
      _BOAS = "-"
      _PEAS = _BHAS22 + " (18/3)"
      _MaAS = _MaAS1 + " (16/6)"
      _EnAS = _EnAS3 + " (14/6)"
      _WiAS = _WiAS2 + " (15/3)"
      _NaAS = "-"
      _DeAS = _DeAS2 + " (10/6)"
      _ReAS = _ReAS3 + " (7/3)"
      _SpAS = _SpAS1 + " (8/3)"
      _PoAS = _PoAS2 + " (8/3)"
      _BOLS = "-"
      _PELS = "link:LS-" + asbh22 + "{ext-relative}[LS zu AS{nbsp}-{nbsp}2.2]"
      _MaLS = "link:LS-" + asma1 + "{ext-relative}[LS zu AS{nbsp}-{nbsp}1]"
      _EnLS = "link:LS-" + asen3 + "{ext-relative}[LS zu AS{nbsp}-{nbsp}3]"
      _WiLS = "link:LS-" + aswi2 + "{ext-relative}[LS zu AS{nbsp}-{nbsp}2]"
      _NaLS = "-"
      _DeLS = "link:LS-" + asde2 + "{ext-relative}[LS zu AS{nbsp}-{nbsp}2]"
      _ReLS = "link:LS-" + asre3 + "{ext-relative}[LS zu AS{nbsp}-{nbsp}3]"
      _SpLS = "link:LS-" + assp1 + "{ext-relative}[LS zu AS{nbsp}-{nbsp}1]"
      _PoLS = "link:LS-" + aspo2 + "{ext-relative}[LS zu AS{nbsp}-{nbsp}2]"
      array_week_AS[i] = ["begin AS in week " +i.to_s,_BOAS,_PEAS,_MaAS,_EnAS,_WiAS,_NaAS,_DeAS,_ReAS,_SpAS,_PoAS,"end AS in week " +i.to_s]
      array_week_LS[i] = ["begin LS in week " +i.to_s,_BOLS,_PELS,_MaLS,_EnLS,_WiLS,_NaLS,_DeLS,_ReLS,_SpLS,_PoLS,"end LS in week " +i.to_s]

    when 4
      i=i+1
      _BOAS = "-"
      _PEAS = _BHAS22 + " (18/6)"
      _MaAS = _MaAS1 + " (16/8)"
      _EnAS = _EnAS3 + " (14/8)"
      _WiAS = _WiAS2 + " (15/4)"
      _NaAS = "-"
      _DeAS = _DeAS2 + " (10/8)"
      _ReAS = _ReAS3 + " (7/4)"
      _SpAS = _SpAS1 + " (8/4)"
      _PoAS = _PoAS2 + " (8/4)"
      _BOLS = "-"
      _PELS = "link:LS-" + asbh22 + "{ext-relative}[LS zu AS{nbsp}-{nbsp}2.2]"
      _MaLS = "link:LS-" + asma1 + "{ext-relative}[LS zu AS{nbsp}-{nbsp}1]"
      _EnLS = "link:LS-" + asen3 + "{ext-relative}[LS zu AS{nbsp}-{nbsp}3]"
      _WiLS = "link:LS-" + aswi2 + "{ext-relative}[LS zu AS{nbsp}-{nbsp}2]"
      _NaLS = "-"
      _DeLS = "link:LS-" + asde2 + "{ext-relative}[LS zu AS{nbsp}-{nbsp}2]"
      _ReLS = "link:LS-" + asre3 + "{ext-relative}[LS zu AS{nbsp}-{nbsp}3]"
      _SpLS = "link:LS-" + assp1 + "{ext-relative}[LS zu AS{nbsp}-{nbsp}1]"
      _PoLS = "link:LS-" + aspo2 + "{ext-relative}[LS zu AS{nbsp}-{nbsp}2]"
      array_week_AS[i] = ["begin AS in week " +i.to_s,_BOAS,_PEAS,_MaAS,_EnAS,_WiAS,_NaAS,_DeAS,_ReAS,_SpAS,_PoAS,"end AS in week " +i.to_s]
      array_week_LS[i] = ["begin LS in week " +i.to_s,_BOLS,_PELS,_MaLS,_EnLS,_WiLS,_NaLS,_DeLS,_ReLS,_SpLS,_PoLS,"end LS in week " +i.to_s]

    when 5
      i=i+1
      _BOAS = "-"
      _PEAS = _BHAS22 + " (18/9)"
      _MaAS = _MaAS1 + " (16/10)"
      _EnAS = _EnAS3 + " (14/10)"
      _WiAS = _WiAS2 + " (15/5)"
      _NaAS = "-"
      _DeAS = _DeAS2 + " (10/10)"
      _ReAS = _ReAS3 + " (7/5)"
      _SpAS = _SpAS1 + " (8/5)"
      _PoAS = _PoAS2 + " (8/5)"
      _BOLS = "-"
      _PELS = "link:LS-" + asbh22 + "{ext-relative}[LS zu AS{nbsp}-{nbsp}2.2]"
      _MaLS = "link:LS-" + asma1 + "{ext-relative}[LS zu AS{nbsp}-{nbsp}1]"
      _EnLS = "link:LS-" + asen3 + "{ext-relative}[LS zu AS{nbsp}-{nbsp}3]"
      _WiLS = "link:LS-" + aswi2 + "{ext-relative}[LS zu AS{nbsp}-{nbsp}2]"
      _NaLS = "-"
      _DeLS = "link:LS-" + asde2 + "{ext-relative}[LS zu AS{nbsp}-{nbsp}2]"
      _ReLS = "link:LS-" + asre3 + "{ext-relative}[LS zu AS{nbsp}-{nbsp}3]"
      _SpLS = "link:LS-" + assp1 + "{ext-relative}[LS zu AS{nbsp}-{nbsp}1]"
      _PoLS = "link:LS-" + aspo2 + "{ext-relative}[LS zu AS{nbsp}-{nbsp}2]"
      array_week_AS[i] = ["begin AS in week " +i.to_s,_BOAS,_PEAS,_MaAS,_EnAS,_WiAS,_NaAS,_DeAS,_ReAS,_SpAS,_PoAS,"end AS in week " +i.to_s]
      array_week_LS[i] = ["begin LS in week " +i.to_s,_BOLS,_PELS,_MaLS,_EnLS,_WiLS,_NaLS,_DeLS,_ReLS,_SpLS,_PoLS,"end LS in week " +i.to_s]

    when 6
      i=i+1
      _BOAS = "-"
      _PEAS = _BHAS22 + " (18/12)"
      _MaAS = _MaAS1 + " (16/12)"
      _EnAS = _EnAS3 + " (14/12)"
      _WiAS = _WiAS2 + " (15/6)"
      _NaAS = "-"
      _DeAS = _DeAS1 + " (20/2)"
      _ReAS = _ReAS3 + " (7/6)"
      _SpAS = _SpAS1 + " (8/6)"
      _PoAS = _PoAS2 + " (8/6)"
      _BOLS = "-"
      _PELS = "link:LS-" + asbh22 + "{ext-relative}[LS zu AS{nbsp}-{nbsp}2.2]"
      _MaLS = "link:LS-" + asma1 + "{ext-relative}[LS zu AS{nbsp}-{nbsp}1]"
      _EnLS = "link:LS-" + asen3 + "{ext-relative}[LS zu AS{nbsp}-{nbsp}3]"
      _WiLS = "link:LS-" + aswi2 + "{ext-relative}[LS zu AS{nbsp}-{nbsp}2]"
      _NaLS = "-"
      _DeLS = "link:LS-" + asde1 + "{ext-relative}[LS zu AS{nbsp}-{nbsp}1]"
      _ReLS = "link:LS-" + asre3 + "{ext-relative}[LS zu AS{nbsp}-{nbsp}3]"
      _SpLS = "link:LS-" + assp1 + "{ext-relative}[LS zu AS{nbsp}-{nbsp}1]"
      _PoLS = "link:LS-" + aspo2 + "{ext-relative}[LS zu AS{nbsp}-{nbsp}2]"
      array_week_AS[i] = ["begin AS in week " +i.to_s,_BOAS,_PEAS,_MaAS,_EnAS,_WiAS,_NaAS,_DeAS,_ReAS,_SpAS,_PoAS,"end AS in week " +i.to_s]
      array_week_LS[i] = ["begin LS in week " +i.to_s,_BOLS,_PELS,_MaLS,_EnLS,_WiLS,_NaLS,_DeLS,_ReLS,_SpLS,_PoLS,"end LS in week " +i.to_s]

    when 7
      i=i+1
      _BOAS = "-"
      _PEAS = _BHAS22 + " (18/15)"
      _MaAS = _MaAS1 + " (16/14)"
      _EnAS = _EnAS3 + " (14/14)"
      _WiAS = _WiAS2 + " (15/7)"
      _NaAS = "-"
      _DeAS = _DeAS1 + " (20/4)"
      _ReAS = _ReAS3 + " (7/7)"
      _SpAS = _SpAS1 + " (8/7)"
      _PoAS = _PoAS2 + " (8/7)"
      _BOLS = "-"
      _PELS = "link:LS-" + asbh22 + "{ext-relative}[LS zu AS{nbsp}-{nbsp}2.2]"
      _MaLS = "link:LS-" + asma1 + "{ext-relative}[LS zu AS{nbsp}-{nbsp}1]"
      _EnLS = "link:LS-" + asen3 + "{ext-relative}[LS zu AS{nbsp}-{nbsp}3]"
      _WiLS = "link:LS-" + aswi2 + "{ext-relative}[LS zu AS{nbsp}-{nbsp}2]"
      _NaLS = "-"
      _DeLS = "link:LS-" + asde1 + "{ext-relative}[LS zu AS{nbsp}-{nbsp}1]"
      _ReLS = "link:LS-" + asre3 + "{ext-relative}[LS zu AS{nbsp}-{nbsp}3]"
      _SpLS = "link:LS-" + assp1 + "{ext-relative}[LS zu AS{nbsp}-{nbsp}1]"
      _PoLS = "link:LS-" + aspo2 + "{ext-relative}[LS zu AS{nbsp}-{nbsp}2]"
      array_week_AS[i] = ["begin AS in week " +i.to_s,_BOAS,_PEAS,_MaAS,_EnAS,_WiAS,_NaAS,_DeAS,_ReAS,_SpAS,_PoAS,"end AS in week " +i.to_s]
      array_week_LS[i] = ["begin LS in week " +i.to_s,_BOLS,_PELS,_MaLS,_EnLS,_WiLS,_NaLS,_DeLS,_ReLS,_SpLS,_PoLS,"end LS in week " +i.to_s]

    when 8
      i=i+1
      _BOAS = "-"
      _PEAS = _BHAS22 + " (18/18)"
      _MaAS = _MaAS1 + " (16/16)"
      _EnAS = _EnAS2 + " (20/2)"
      _WiAS = _WiAS2 + " (15/8)"
      _NaAS = "-"
      _DeAS = _DeAS1 + " (20/6)"
      _ReAS = _ReAS1 + " (7/1)"
      _SpAS = _SpAS1 + " (8/8)"
      _PoAS = _PoAS2 + " (8/8)"
      _BOLS = "-"
      _PELS = "link:LS-" + asbh22 + "{ext-relative}[LS zu AS{nbsp}-{nbsp}2.2]"
      _MaLS = "link:LS-" + asma1 + "{ext-relative}[LS zu AS{nbsp}-{nbsp}1]"
      _EnLS = "link:LS-" + asen2 + "{ext-relative}[LS zu AS{nbsp}-{nbsp}2]"
      _WiLS = "link:LS-" + aswi2 + "{ext-relative}[LS zu AS{nbsp}-{nbsp}2]"
      _NaLS = "-"
      _DeLS = "link:LS-" + asde1 + "{ext-relative}[LS zu AS{nbsp}-{nbsp}1]"
      _ReLS = "link:LS-" + asre1 + "{ext-relative}[LS zu AS{nbsp}-{nbsp}1]"
      _SpLS = "link:LS-" + assp1 + "{ext-relative}[LS zu AS{nbsp}-{nbsp}1]"
      _PoLS = "link:LS-" + aspo2 + "{ext-relative}[LS zu AS{nbsp}-{nbsp}2]"
      array_week_AS[i] = ["begin AS in week " +i.to_s,_BOAS,_PEAS,_MaAS,_EnAS,_WiAS,_NaAS,_DeAS,_ReAS,_SpAS,_PoAS,"end AS in week " +i.to_s]
      array_week_LS[i] = ["begin LS in week " +i.to_s,_BOLS,_PELS,_MaLS,_EnLS,_WiLS,_NaLS,_DeLS,_ReLS,_SpLS,_PoLS,"end LS in week " +i.to_s]
   
    when 9
      i=i+1
      _BOAS = "-"
      _PEAS = _BHAS23 + " (9/3)"
      _MaAS = _MaAS3 + " (24/2)"
      _EnAS = _EnAS2 + " (20/4)"
      _WiAS = _WiAS2 + " (15/9)"
      _NaAS = "-"
      _DeAS = _DeAS1 + " (20/8)"
      _ReAS = _ReAS1 + " (7/2)"
      _SpAS = _SpAS2 + " (6/1)"
      _PoAS = _PoAS1 + " (8/1)"
      _BOLS = "-"
      _PELS = "link:LS-" + asbh23 + "{ext-relative}[LS zu AS{nbsp}-{nbsp}2.3]"
      _MaLS = "link:LS-" + asma3 + "{ext-relative}[LS zu AS{nbsp}-{nbsp}3]"
      _EnLS = "link:LS-" + asen2 + "{ext-relative}[LS zu AS{nbsp}-{nbsp}2]"
      _WiLS = "link:LS-" + aswi2 + "{ext-relative}[LS zu AS{nbsp}-{nbsp}2]"
      _NaLS = "-"
      _DeLS = "link:LS-" + asde1 + "{ext-relative}[LS zu AS{nbsp}-{nbsp}1]"
      _ReLS = "link:LS-" + asre1 + "{ext-relative}[LS zu AS{nbsp}-{nbsp}1]"
      _SpLS = "link:LS-" + assp2 + "{ext-relative}[LS zu AS{nbsp}-{nbsp}2]"
      _PoLS = "link:LS-" + aspo1 + "{ext-relative}[LS zu AS{nbsp}-{nbsp}1]"
      array_week_AS[i] = ["begin AS in week " +i.to_s,_BOAS,_PEAS,_MaAS,_EnAS,_WiAS,_NaAS,_DeAS,_ReAS,_SpAS,_PoAS,"end AS in week " +i.to_s]
      array_week_LS[i] = ["begin LS in week " +i.to_s,_BOLS,_PELS,_MaLS,_EnLS,_WiLS,_NaLS,_DeLS,_ReLS,_SpLS,_PoLS,"end LS in week " +i.to_s]

    when 10
      i=i+1
      _BOAS = "-"
      _PEAS = _BHAS23 + " (9/6)"
      _MaAS = _MaAS3 + " (24/4)"
      _EnAS = _EnAS2 + " (20/6)"
      _WiAS = _WiAS2 + " (15/10)"
      _NaAS = "-"
      _DeAS = _DeAS1 + " (20/10)"
      _ReAS = _ReAS1 + " (7/3)"
      _SpAS = _SpAS2 + " (6/2)"
      _PoAS = _PoAS1 + " (8/2)"
      _BOLS = "-"
      _PELS = "link:LS-" + asbh23 + "{ext-relative}[LS zu AS{nbsp}-{nbsp}2.3]"
      _MaLS = "link:LS-" + asma3 + "{ext-relative}[LS zu AS{nbsp}-{nbsp}3]"
      _EnLS = "link:LS-" + asen2 + "{ext-relative}[LS zu AS{nbsp}-{nbsp}2]"
      _WiLS = "link:LS-" + aswi2 + "{ext-relative}[LS zu AS{nbsp}-{nbsp}2]"
      _NaLS = "-"
      _DeLS = "link:LS-" + asde1 + "{ext-relative}[LS zu AS{nbsp}-{nbsp}1]"
      _ReLS = "link:LS-" + asre1 + "{ext-relative}[LS zu AS{nbsp}-{nbsp}1]"
      _SpLS = "link:LS-" + assp2 + "{ext-relative}[LS zu AS{nbsp}-{nbsp}2]"
      _PoLS = "link:LS-" + aspo1 + "{ext-relative}[LS zu AS{nbsp}-{nbsp}1]"
      array_week_AS[i] = ["begin AS in week " +i.to_s,_BOAS,_PEAS,_MaAS,_EnAS,_WiAS,_NaAS,_DeAS,_ReAS,_SpAS,_PoAS,"end AS in week " +i.to_s]
      array_week_LS[i] = ["begin LS in week " +i.to_s,_BOLS,_PELS,_MaLS,_EnLS,_WiLS,_NaLS,_DeLS,_ReLS,_SpLS,_PoLS,"end LS in week " +i.to_s]

    when 11
      i=i+1
      _BOAS = "-"
      _PEAS = _BHAS23 + " (9/9)"
      _MaAS = _MaAS3 + " (24/6)"
      _EnAS = _EnAS2 + " (20/8)"
      _WiAS = _WiAS2 + " (15/11)"
      _NaAS = "-"
      _DeAS = _DeAS1 + " (20/12)"
      _ReAS = _ReAS1 + " (7/4)"
      _SpAS = _SpAS2 + " (6/3)"
      _PoAS = _PoAS1 + " (8/3)"
      _BOLS = "-"
      _PELS = "link:LS-" + asbh23 + "{ext-relative}[LS zu AS{nbsp}-{nbsp}2.3]"
      _MaLS = "link:LS-" + asma3 + "{ext-relative}[LS zu AS{nbsp}-{nbsp}3]"
      _EnLS = "link:LS-" + asen2 + "{ext-relative}[LS zu AS{nbsp}-{nbsp}2]"
      _WiLS = "link:LS-" + aswi2 + "{ext-relative}[LS zu AS{nbsp}-{nbsp}2]"
      _NaLS = "-"
      _DeLS = "link:LS-" + asde1 + "{ext-relative}[LS zu AS{nbsp}-{nbsp}1]"
      _ReLS = "link:LS-" + asre1 + "{ext-relative}[LS zu AS{nbsp}-{nbsp}1]"
      _SpLS = "link:LS-" + assp2 + "{ext-relative}[LS zu AS{nbsp}-{nbsp}2]"
      _PoLS = "link:LS-" + aspo1 + "{ext-relative}[LS zu AS{nbsp}-{nbsp}1]"
      array_week_AS[i] = ["begin AS in week " +i.to_s,_BOAS,_PEAS,_MaAS,_EnAS,_WiAS,_NaAS,_DeAS,_ReAS,_SpAS,_PoAS,"end AS in week " +i.to_s]
      array_week_LS[i] = ["begin LS in week " +i.to_s,_BOLS,_PELS,_MaLS,_EnLS,_WiLS,_NaLS,_DeLS,_ReLS,_SpLS,_PoLS,"end LS in week " +i.to_s]

    when 12
      i=i+1
      _BOAS = "-"
      _PEAS = _BHAS24 + " (21/3)"
      _MaAS = _MaAS3 + " (24/8)"
      _EnAS = _EnAS2 + " (20/10)"
      _WiAS = _WiAS2 + " (15/12)"
      _NaAS = "-"
      _DeAS = _DeAS1 + " (20/14)"
      _ReAS = _ReAS1 + " (7/5)"
      _SpAS = _SpAS2 + " (6/4)"
      _PoAS = _PoAS1 + " (8/4)"
      _BOLS = "-"
      _PELS = "link:LS-" + asbh24 + "{ext-relative}[LS zu AS{nbsp}-{nbsp}2.4]"
      _MaLS = "link:LS-" + asma3 + "{ext-relative}[LS zu AS{nbsp}-{nbsp}3]"
      _EnLS = "link:LS-" + asen2 + "{ext-relative}[LS zu AS{nbsp}-{nbsp}2]"
      _WiLS = "link:LS-" + aswi2 + "{ext-relative}[LS zu AS{nbsp}-{nbsp}2]"
      _NaLS = "-"
      _DeLS = "link:LS-" + asde1 + "{ext-relative}[LS zu AS{nbsp}-{nbsp}1]"
      _ReLS = "link:LS-" + asre1 + "{ext-relative}[LS zu AS{nbsp}-{nbsp}1]"
      _SpLS = "link:LS-" + assp2 + "{ext-relative}[LS zu AS{nbsp}-{nbsp}2]"
      _PoLS = "link:LS-" + aspo1 + "{ext-relative}[LS zu AS{nbsp}-{nbsp}1]"
      array_week_AS[i] = ["begin AS in week " +i.to_s,_BOAS,_PEAS,_MaAS,_EnAS,_WiAS,_NaAS,_DeAS,_ReAS,_SpAS,_PoAS,"end AS in week " +i.to_s]
      array_week_LS[i] = ["begin LS in week " +i.to_s,_BOLS,_PELS,_MaLS,_EnLS,_WiLS,_NaLS,_DeLS,_ReLS,_SpLS,_PoLS,"end LS in week " +i.to_s]

    when 13
      i=i+1
      _BOAS = "-"
      _PEAS = _BHAS24 + " (21/6)"
      _MaAS = _MaAS3 + " (24/10)"
      _EnAS = _EnAS2 + " (20/12)"
      _WiAS = _WiAS2 + " (15/13)"
      _NaAS = "-"
      _DeAS = _DeAS1 + " (20/16)"
      _ReAS = _ReAS1 + " (7/6)"
      _SpAS = _SpAS2 + " (6/5)"
      _PoAS = _PoAS1 + " (8/5)"
      _BOLS = "-"
      _PELS = "link:LS-" + asbh24 + "{ext-relative}[LS zu AS{nbsp}-{nbsp}2.4]"
      _MaLS = "link:LS-" + asma3 + "{ext-relative}[LS zu AS{nbsp}-{nbsp}3]"
      _EnLS = "link:LS-" + asen2 + "{ext-relative}[LS zu AS{nbsp}-{nbsp}2]"
      _WiLS = "link:LS-" + aswi2 + "{ext-relative}[LS zu AS{nbsp}-{nbsp}2]"
      _NaLS = "-"
      _DeLS = "link:LS-" + asde1 + "{ext-relative}[LS zu AS{nbsp}-{nbsp}1]"
      _ReLS = "link:LS-" + asre1 + "{ext-relative}[LS zu AS{nbsp}-{nbsp}1]"
      _SpLS = "link:LS-" + assp2 + "{ext-relative}[LS zu AS{nbsp}-{nbsp}2]"
      _PoLS = "link:LS-" + aspo1 + "{ext-relative}[LS zu AS{nbsp}-{nbsp}1]"
      array_week_AS[i] = ["begin AS in week " +i.to_s,_BOAS,_PEAS,_MaAS,_EnAS,_WiAS,_NaAS,_DeAS,_ReAS,_SpAS,_PoAS,"end AS in week " +i.to_s]
      array_week_LS[i] = ["begin LS in week " +i.to_s,_BOLS,_PELS,_MaLS,_EnLS,_WiLS,_NaLS,_DeLS,_ReLS,_SpLS,_PoLS,"end LS in week " +i.to_s]

    when 14
      i=i+1
      _BOAS = "-"
      _PEAS = _BHAS24 + " (21/9)"
      _MaAS = _MaAS3 + " (24/12)"
      _EnAS = _EnAS2 + " (20/14)"
      _WiAS = _WiAS2 + " (15/14)"
      _NaAS = "-"
      _DeAS = _DeAS1 + " (20/18)"
      _ReAS = _ReAS1 + " (7/7)"
      _SpAS = _SpAS2 + " (6/6)"
      _PoAS = _PoAS1 + " (8/6)"
      _BOLS = "-"
      _PELS = "link:LS-" + asbh24 + "{ext-relative}[LS zu AS{nbsp}-{nbsp}2.4]"
      _MaLS = "link:LS-" + asma3 + "{ext-relative}[LS zu AS{nbsp}-{nbsp}3]"
      _EnLS = "link:LS-" + asen2 + "{ext-relative}[LS zu AS{nbsp}-{nbsp}2]"
      _WiLS = "link:LS-" + aswi2 + "{ext-relative}[LS zu AS{nbsp}-{nbsp}2]"
      _NaLS = "-"
      _DeLS = "link:LS-" + asde1 + "{ext-relative}[LS zu AS{nbsp}-{nbsp}1]"
      _ReLS = "link:LS-" + asre1 + "{ext-relative}[LS zu AS{nbsp}-{nbsp}1]"
      _SpLS = "link:LS-" + assp2 + "{ext-relative}[LS zu AS{nbsp}-{nbsp}2]"
      _PoLS = "link:LS-" + aspo1 + "{ext-relative}[LS zu AS{nbsp}-{nbsp}1]"
      array_week_AS[i] = ["begin AS in week " +i.to_s,_BOAS,_PEAS,_MaAS,_EnAS,_WiAS,_NaAS,_DeAS,_ReAS,_SpAS,_PoAS,"end AS in week " +i.to_s]
      array_week_LS[i] = ["begin LS in week " +i.to_s,_BOLS,_PELS,_MaLS,_EnLS,_WiLS,_NaLS,_DeLS,_ReLS,_SpLS,_PoLS,"end LS in week " +i.to_s]

    when 15
      i=i+1
      _BOAS = "-"
      _PEAS = _BHAS24 + " (21/12)"
      _MaAS = _MaAS3 + " (24/14)"
      _EnAS = _EnAS2 + " (20/16)"
      _WiAS = _WiAS2 + " (15/15)"
      _NaAS = "-"
      _DeAS = _DeAS1 + " (20/20)"
      _ReAS = _ReAS2 + " (6/1)"
      _SpAS = _SpAS6 + " (6/1)"
      _PoAS = _PoAS1 + " (8/7)"
      _BOLS = "-"
      _PELS = "link:LS-" + asbh24 + "{ext-relative}[LS zu AS{nbsp}-{nbsp}2.4]"
      _MaLS = "link:LS-" + asma3 + "{ext-relative}[LS zu AS{nbsp}-{nbsp}3]"
      _EnLS = "link:LS-" + asen2 + "{ext-relative}[LS zu AS{nbsp}-{nbsp}2]"
      _WiLS = "link:LS-" + aswi2 + "{ext-relative}[LS zu AS{nbsp}-{nbsp}2]"
      _NaLS = "-"
      _DeLS = "link:LS-" + asde1 + "{ext-relative}[LS zu AS{nbsp}-{nbsp}1]"
      _ReLS = "link:LS-" + asre2 + "{ext-relative}[LS zu AS{nbsp}-{nbsp}2]"
      _SpLS = "link:LS-" + assp6 + "{ext-relative}[LS zu AS{nbsp}-{nbsp}6]"
      _PoLS = "link:LS-" + aspo1 + "{ext-relative}[LS zu AS{nbsp}-{nbsp}1]"
      array_week_AS[i] = ["begin AS in week " +i.to_s,_BOAS,_PEAS,_MaAS,_EnAS,_WiAS,_NaAS,_DeAS,_ReAS,_SpAS,_PoAS,"end AS in week " +i.to_s]
      array_week_LS[i] = ["begin LS in week " +i.to_s,_BOLS,_PELS,_MaLS,_EnLS,_WiLS,_NaLS,_DeLS,_ReLS,_SpLS,_PoLS,"end LS in week " +i.to_s]

    when 16
      i=i+1
      _BOAS = "-"
      _PEAS = _BHAS24 + " (21/15)"
      _MaAS = _MaAS3 + " (24/16)"
      _EnAS = _EnAS2 + " (20/18)"
      _WiAS = _WiAS3 + " (10/1)"
      _NaAS = "-"
      _DeAS = _DeAS5 + " (20/2)"
      _ReAS = _ReAS2 + " (6/2)"
      _SpAS = _SpAS6 + " (6/2)"
      _PoAS = _PoAS1 + " (8/8)"
      _BOLS = "-"
      _PELS = "link:LS-" + asbh24 + "{ext-relative}[LS zu AS{nbsp}-{nbsp}2.4]"
      _MaLS = "link:LS-" + asma3 + "{ext-relative}[LS zu AS{nbsp}-{nbsp}3]"
      _EnLS = "link:LS-" + asen2 + "{ext-relative}[LS zu AS{nbsp}-{nbsp}2]"
      _WiLS = "link:LS-" + aswi3 + "{ext-relative}[LS zu AS{nbsp}-{nbsp}3]"
      _NaLS = "-"
      _DeLS = "link:LS-" + asde5 + "{ext-relative}[LS zu AS{nbsp}-{nbsp}5]"
      _ReLS = "link:LS-" + asre2 + "{ext-relative}[LS zu AS{nbsp}-{nbsp}2]"
      _SpLS = "link:LS-" + assp6 + "{ext-relative}[LS zu AS{nbsp}-{nbsp}6]"
      _PoLS = "link:LS-" + aspo1 + "{ext-relative}[LS zu AS{nbsp}-{nbsp}1]"
      array_week_AS[i] = ["begin AS in week " +i.to_s,_BOAS,_PEAS,_MaAS,_EnAS,_WiAS,_NaAS,_DeAS,_ReAS,_SpAS,_PoAS,"end AS in week " +i.to_s]
      array_week_LS[i] = ["begin LS in week " +i.to_s,_BOLS,_PELS,_MaLS,_EnLS,_WiLS,_NaLS,_DeLS,_ReLS,_SpLS,_PoLS,"end LS in week " +i.to_s]

    when 17
      i=i+1
      _BOAS = "-"
      _PEAS = _BHAS24 + " (21/18)"
      _MaAS = _MaAS3 + " (24/18)"
      _EnAS = _EnAS2 + " (20/20)"
      _WiAS = _WiAS3 + " (10/2)"
      _NaAS = "-"
      _DeAS = _DeAS5 + " (20/4)"
      _ReAS = _ReAS2 + " (6/3)"
      _SpAS = _SpAS6 + " (6/3)"
      _PoAS = _PoAS4 + " (15/1)"
      _BOLS = "-"
      _PELS = "link:LS-" + asbh24 + "{ext-relative}[LS zu AS{nbsp}-{nbsp}2.4]"
      _MaLS = "link:LS-" + asma3 + "{ext-relative}[LS zu AS{nbsp}-{nbsp}3]"
      _EnLS = "link:LS-" + asen2 + "{ext-relative}[LS zu AS{nbsp}-{nbsp}2]"
      _WiLS = "link:LS-" + aswi3 + "{ext-relative}[LS zu AS{nbsp}-{nbsp}3]"
      _NaLS = "-"
      _DeLS = "link:LS-" + asde5 + "{ext-relative}[LS zu AS{nbsp}-{nbsp}5]"
      _ReLS = "link:LS-" + asre2 + "{ext-relative}[LS zu AS{nbsp}-{nbsp}2]"
      _SpLS = "link:LS-" + assp3 + "{ext-relative}[LS zu AS{nbsp}-{nbsp}3]"
      _PoLS = "link:LS-" + aspo4 + "{ext-relative}[LS zu AS{nbsp}-{nbsp}4]"
      array_week_AS[i] = ["begin AS in week " +i.to_s,_BOAS,_PEAS,_MaAS,_EnAS,_WiAS,_NaAS,_DeAS,_ReAS,_SpAS,_PoAS,"end AS in week " +i.to_s]
      array_week_LS[i] = ["begin LS in week " +i.to_s,_BOLS,_PELS,_MaLS,_EnLS,_WiLS,_NaLS,_DeLS,_ReLS,_SpLS,_PoLS,"end LS in week " +i.to_s]

    when 18
      i=i+1
      _BOAS = "-"
      _PEAS = _BHAS24 + " (21/21)"
      _MaAS = _MaAS3 + " (24/20)"
      _EnAS = _EnAS4 + " (20/2)"
      _WiAS = _WiAS3 + " (10/3)"
      _NaAS = "-"
      _DeAS = _DeAS5 + " (20/6)"
      _ReAS = _ReAS2 + " (6/4)"
      _SpAS = _SpAS6 + " (6/4)"
      _PoAS = _PoAS4 + " (15/2)"
      _BOLS = "-"
      _PELS = "link:LS-" + asbh24 + "{ext-relative}[LS zu AS{nbsp}-{nbsp}2.4]"
      _MaLS = "link:LS-" + asma3 + "{ext-relative}[LS zu AS{nbsp}-{nbsp}3]"
      _EnLS = "link:LS-" + asen4 + "{ext-relative}[LS zu AS{nbsp}-{nbsp}4]"
      _WiLS = "link:LS-" + aswi3 + "{ext-relative}[LS zu AS{nbsp}-{nbsp}3]"
      _NaLS = "-"
      _DeLS = "link:LS-" + asde5 + "{ext-relative}[LS zu AS{nbsp}-{nbsp}5]"
      _ReLS = "link:LS-" + asre2 + "{ext-relative}[LS zu AS{nbsp}-{nbsp}2]"
      _SpLS = "link:LS-" + assp6 + "{ext-relative}[LS zu AS{nbsp}-{nbsp}6]"
      _PoLS = "link:LS-" + aspo4 + "{ext-relative}[LS zu AS{nbsp}-{nbsp}4]"
      array_week_AS[i] = ["begin AS in week " +i.to_s,_BOAS,_PEAS,_MaAS,_EnAS,_WiAS,_NaAS,_DeAS,_ReAS,_SpAS,_PoAS,"end AS in week " +i.to_s]
      array_week_LS[i] = ["begin LS in week " +i.to_s,_BOLS,_PELS,_MaLS,_EnLS,_WiLS,_NaLS,_DeLS,_ReLS,_SpLS,_PoLS,"end LS in week " +i.to_s]
   
    when 19
      i=i+1
      _BOAS = _BHAS11 + " (6/3)"
      _PEAS = "-"
      _MaAS = _MaAS3 + " (24/22)"
      _EnAS = _EnAS4 + " (20/4)"
      _WiAS = _WiAS3 + " (10/4)"
      _NaAS = "-"
      _DeAS = _DeAS5 + " (20/8)"
      _ReAS = _ReAS2 + " (6/5)"
      _SpAS = _SpAS6 + " (6/5)"
      _PoAS = _PoAS4 + " (15/3)"
      _BOLS = "link:LS-" + asbh11 + "{ext-relative}[LS zu AS{nbsp}-{nbsp}1.1]"
      _PELS = "-"
      _MaLS = "link:LS-" + asma3 + "{ext-relative}[LS zu AS{nbsp}-{nbsp}3]"
      _EnLS = "link:LS-" + asen4 + "{ext-relative}[LS zu AS{nbsp}-{nbsp}4]"
      _WiLS = "link:LS-" + aswi3 + "{ext-relative}[LS zu AS{nbsp}-{nbsp}3]"
      _NaLS = "-"
      _DeLS = "link:LS-" + asde5 + "{ext-relative}[LS zu AS{nbsp}-{nbsp}5]"
      _ReLS = "link:LS-" + asre2 + "{ext-relative}[LS zu AS{nbsp}-{nbsp}2]"
      _SpLS = "link:LS-" + assp6 + "{ext-relative}[LS zu AS{nbsp}-{nbsp}6]"
      _PoLS = "link:LS-" + aspo4 + "{ext-relative}[LS zu AS{nbsp}-{nbsp}4]"
      array_week_AS[i] = ["begin AS in week " +i.to_s,_BOAS,_PEAS,_MaAS,_EnAS,_WiAS,_NaAS,_DeAS,_ReAS,_SpAS,_PoAS,"end AS in week " +i.to_s]
      array_week_LS[i] = ["begin LS in week " +i.to_s,_BOLS,_PELS,_MaLS,_EnLS,_WiLS,_NaLS,_DeLS,_ReLS,_SpLS,_PoLS,"end LS in week " +i.to_s]

    when 20
      i=i+1
      _BOAS = _BHAS11 + " (6/6)"
      _PEAS = "-"
      _MaAS = _MaAS3 + " (24/24)"
      _EnAS = _EnAS4 + " (20/6)"
      _WiAS = _WiAS3 + " (10/5)"
      _NaAS = "-"
      _DeAS = _DeAS5 + " (20/10)"
      _ReAS = _ReAS2 + " (6/6)"
      _SpAS = _SpAS6 + " (6/6)"
      _PoAS = _PoAS4 + " (15/4)"
      _BOLS = "link:LS-" + asbh11 + "{ext-relative}[LS zu AS{nbsp}-{nbsp}1.1]"
      _PELS = "-"
      _MaLS = "link:LS-" + asma3 + "{ext-relative}[LS zu AS{nbsp}-{nbsp}3]"
      _EnLS = "link:LS-" + asen4 + "{ext-relative}[LS zu AS{nbsp}-{nbsp}4]"
      _WiLS = "link:LS-" + aswi3 + "{ext-relative}[LS zu AS{nbsp}-{nbsp}3]"
      _NaLS = "-"
      _DeLS = "link:LS-" + asde5 + "{ext-relative}[LS zu AS{nbsp}-{nbsp}5]"
      _ReLS = "link:LS-" + asre2 + "{ext-relative}[LS zu AS{nbsp}-{nbsp}2]"
      _SpLS = "link:LS-" + assp6 + "{ext-relative}[LS zu AS{nbsp}-{nbsp}6]"
      _PoLS = "link:LS-" + aspo4 + "{ext-relative}[LS zu AS{nbsp}-{nbsp}4]"
      array_week_AS[i] = ["begin AS in week " +i.to_s,_BOAS,_PEAS,_MaAS,_EnAS,_WiAS,_NaAS,_DeAS,_ReAS,_SpAS,_PoAS,"end AS in week " +i.to_s]
      array_week_LS[i] = ["begin LS in week " +i.to_s,_BOLS,_PELS,_MaLS,_EnLS,_WiLS,_NaLS,_DeLS,_ReLS,_SpLS,_PoLS,"end LS in week " +i.to_s]

    when 21
      i=i+1
      _BOAS = _BHAS12 + " (6/3)"
      _PEAS = "-"
      _MaAS = _MaAS4 + " (28/2)"
      _EnAS = _EnAS4 + " (20/8)"
      _WiAS = _WiAS3 + " (10/6)"
      _NaAS = "-"
      _DeAS = _DeAS5 + " (20/12)"
      _ReAS = _ReAS4 + " (7/1)"
      _SpAS = _SpAS4 + " (6/1)"
      _PoAS = _PoAS4 + " (15/5)"
      _BOLS = "link:LS-" + asbh12 + "{ext-relative}[LS zu AS{nbsp}-{nbsp}1.2]"
      _PELS = "-"
      _MaLS = "link:LS-" + asma4 + "{ext-relative}[LS zu AS{nbsp}-{nbsp}4]"
      _EnLS = "link:LS-" + asen4 + "{ext-relative}[LS zu AS{nbsp}-{nbsp}4]"
      _WiLS = "link:LS-" + aswi3 + "{ext-relative}[LS zu AS{nbsp}-{nbsp}3]"
      _NaLS = "-"
      _DeLS = "link:LS-" + asde5 + "{ext-relative}[LS zu AS{nbsp}-{nbsp}5]"
      _ReLS = "link:LS-" + asre4 + "{ext-relative}[LS zu AS{nbsp}-{nbsp}4]"
      _SpLS = "link:LS-" + assp4 + "{ext-relative}[LS zu AS{nbsp}-{nbsp}4]"
      _PoLS = "link:LS-" + aspo4 + "{ext-relative}[LS zu AS{nbsp}-{nbsp}4]"
      array_week_AS[i] = ["begin AS in week " +i.to_s,_BOAS,_PEAS,_MaAS,_EnAS,_WiAS,_NaAS,_DeAS,_ReAS,_SpAS,_PoAS,"end AS in week " +i.to_s]
      array_week_LS[i] = ["begin LS in week " +i.to_s,_BOLS,_PELS,_MaLS,_EnLS,_WiLS,_NaLS,_DeLS,_ReLS,_SpLS,_PoLS,"end LS in week " +i.to_s]

    when 22
      i=i+1
      _BOAS = _BHAS12 + " (6/6)"
      _PEAS = "-"
      _MaAS = _MaAS4 + " (28/4)"
      _EnAS = _EnAS4 + " (20/10)"
      _WiAS = _WiAS3 + " (10/7)"
      _NaAS = "-"
      _DeAS = _DeAS5 + " (20/14)"
      _ReAS = _ReAS4 + " (7/2)"
      _SpAS = _SpAS4 + " (6/2)"
      _PoAS = _PoAS4 + " (15/6)"
      _BOLS = "link:LS-" + asbh12 + "{ext-relative}[LS zu AS{nbsp}-{nbsp}1.2]"
      _PELS = "-"
      _MaLS = "link:LS-" + asma4 + "{ext-relative}[LS zu AS{nbsp}-{nbsp}4]"
      _EnLS = "link:LS-" + asen4 + "{ext-relative}[LS zu AS{nbsp}-{nbsp}4]"
      _WiLS = "link:LS-" + aswi3 + "{ext-relative}[LS zu AS{nbsp}-{nbsp}3]"
      _NaLS = "-"
      _DeLS = "link:LS-" + asde5 + "{ext-relative}[LS zu AS{nbsp}-{nbsp}5]"
      _ReLS = "link:LS-" + asre4 + "{ext-relative}[LS zu AS{nbsp}-{nbsp}4]"
      _SpLS = "link:LS-" + assp4 + "{ext-relative}[LS zu AS{nbsp}-{nbsp}4]"
      _PoLS = "link:LS-" + aspo4 + "{ext-relative}[LS zu AS{nbsp}-{nbsp}4]"
      array_week_AS[i] = ["begin AS in week " +i.to_s,_BOAS,_PEAS,_MaAS,_EnAS,_WiAS,_NaAS,_DeAS,_ReAS,_SpAS,_PoAS,"end AS in week " +i.to_s]
      array_week_LS[i] = ["begin LS in week " +i.to_s,_BOLS,_PELS,_MaLS,_EnLS,_WiLS,_NaLS,_DeLS,_ReLS,_SpLS,_PoLS,"end LS in week " +i.to_s]

    when 23
      i=i+1
      _BOAS = "-"
      _PEAS = _BHAS31 + " (21/3)"
      _MaAS = _MaAS4 + " (28/6)"
      _EnAS = _EnAS4 + " (20/12)"
      _WiAS = _WiAS3 + " (10/8)"
      _NaAS = "-"
      _DeAS = _DeAS5 + " (20/16)"
      _ReAS = _ReAS4 + " (7/3)"
      _SpAS = _SpAS4 + " (6/3)"
      _PoAS = _PoAS4 + " (15/7)"
      _BOLS = "-"
      _PELS = "link:LS-" + asbh31 + "{ext-relative}[LS zu AS{nbsp}-{nbsp}3.1]"
      _MaLS = "link:LS-" + asma4 + "{ext-relative}[LS zu AS{nbsp}-{nbsp}4]"
      _EnLS = "link:LS-" + asen4 + "{ext-relative}[LS zu AS{nbsp}-{nbsp}4]"
      _WiLS = "link:LS-" + aswi3 + "{ext-relative}[LS zu AS{nbsp}-{nbsp}3]"
      _NaLS = "-"
      _DeLS = "link:LS-" + asde5 + "{ext-relative}[LS zu AS{nbsp}-{nbsp}5]"
      _ReLS = "link:LS-" + asre4 + "{ext-relative}[LS zu AS{nbsp}-{nbsp}4]"
      _SpLS = "link:LS-" + assp4 + "{ext-relative}[LS zu AS{nbsp}-{nbsp}4]"
      _PoLS = "link:LS-" + aspo4 + "{ext-relative}[LS zu AS{nbsp}-{nbsp}4]"
      array_week_AS[i] = ["begin AS in week " +i.to_s,_BOAS,_PEAS,_MaAS,_EnAS,_WiAS,_NaAS,_DeAS,_ReAS,_SpAS,_PoAS,"end AS in week " +i.to_s]
      array_week_LS[i] = ["begin LS in week " +i.to_s,_BOLS,_PELS,_MaLS,_EnLS,_WiLS,_NaLS,_DeLS,_ReLS,_SpLS,_PoLS,"end LS in week " +i.to_s]

    when 24
      i=i+1
      _BOAS = "-"
      _PEAS = _BHAS31 + " (21/6)"
      _MaAS = _MaAS4 + " (28/8)"
      _EnAS = _EnAS4 + " (20/14)"
      _WiAS = _WiAS3 + " (10/9)"
      _NaAS = "-"
      _DeAS = _DeAS5 + " (20/18)"
      _ReAS = _ReAS4 + " (7/4)"
      _SpAS = _SpAS4 + " (6/4)"
      _PoAS = _PoAS4 + " (15/8)"
      _BOLS = "-"
      _PELS = "link:LS-" + asbh31 + "{ext-relative}[LS zu AS{nbsp}-{nbsp}3.1]"
      _MaLS = "link:LS-" + asma4 + "{ext-relative}[LS zu AS{nbsp}-{nbsp}4]"
      _EnLS = "link:LS-" + asen4 + "{ext-relative}[LS zu AS{nbsp}-{nbsp}4]"
      _WiLS = "link:LS-" + aswi3 + "{ext-relative}[LS zu AS{nbsp}-{nbsp}3]"
      _NaLS = "-"
      _DeLS = "link:LS-" + asde5 + "{ext-relative}[LS zu AS{nbsp}-{nbsp}5]"
      _ReLS = "link:LS-" + asre4 + "{ext-relative}[LS zu AS{nbsp}-{nbsp}4]"
      _SpLS = "link:LS-" + assp4 + "{ext-relative}[LS zu AS{nbsp}-{nbsp}4]"
      _PoLS = "link:LS-" + aspo4 + "{ext-relative}[LS zu AS{nbsp}-{nbsp}4]"
      array_week_AS[i] = ["begin AS in week " +i.to_s,_BOAS,_PEAS,_MaAS,_EnAS,_WiAS,_NaAS,_DeAS,_ReAS,_SpAS,_PoAS,"end AS in week " +i.to_s]
      array_week_LS[i] = ["begin LS in week " +i.to_s,_BOLS,_PELS,_MaLS,_EnLS,_WiLS,_NaLS,_DeLS,_ReLS,_SpLS,_PoLS,"end LS in week " +i.to_s]

    when 25
      i=i+1
      _BOAS = "-"
      _PEAS = _BHAS31 + " (21/9)"
      _MaAS = _MaAS4 + " (28/10)"
      _EnAS = _EnAS4 + " (20/16)"
      _WiAS = _WiAS3 + " (10/10)"
      _NaAS = "-"
      _DeAS = _DeAS5 + " (20/20)"
      _ReAS = _ReAS4 + " (7/5)"
      _SpAS = _SpAS4 + " (6/5)"
      _PoAS = _PoAS4 + " (15/9)"
      _BOLS = "-"
      _PELS = "link:LS-" + asbh31 + "{ext-relative}[LS zu AS{nbsp}-{nbsp}3.1]"
      _MaLS = "link:LS-" + asma4 + "{ext-relative}[LS zu AS{nbsp}-{nbsp}4]"
      _EnLS = "link:LS-" + asen4 + "{ext-relative}[LS zu AS{nbsp}-{nbsp}4]"
      _WiLS = "link:LS-" + aswi3 + "{ext-relative}[LS zu AS{nbsp}-{nbsp}3]"
      _NaLS = "-"
      _DeLS = "link:LS-" + asde5 + "{ext-relative}[LS zu AS{nbsp}-{nbsp}5]"
      _ReLS = "link:LS-" + asre4 + "{ext-relative}[LS zu AS{nbsp}-{nbsp}4]"
      _SpLS = "link:LS-" + assp4 + "{ext-relative}[LS zu AS{nbsp}-{nbsp}4]"
      _PoLS = "link:LS-" + aspo4 + "{ext-relative}[LS zu AS{nbsp}-{nbsp}4]"
      array_week_AS[i] = ["begin AS in week " +i.to_s,_BOAS,_PEAS,_MaAS,_EnAS,_WiAS,_NaAS,_DeAS,_ReAS,_SpAS,_PoAS,"end AS in week " +i.to_s]
      array_week_LS[i] = ["begin LS in week " +i.to_s,_BOLS,_PELS,_MaLS,_EnLS,_WiLS,_NaLS,_DeLS,_ReLS,_SpLS,_PoLS,"end LS in week " +i.to_s]

    when 26
      i=i+1
      _BOAS = "-"
      _PEAS = _BHAS31 + " (21/12)"
      _MaAS = _MaAS4 + " (28/12)"
      _EnAS = _EnAS4 + " (20/18)"
      _WiAS = _WiAS1 + " (15/1)"
      _NaAS = "-"
      _DeAS = _DeAS3 + " (20/2)"
      _ReAS = _ReAS4 + " (7/6)"
      _SpAS = _SpAS4 + " (6/6)"
      _PoAS = _PoAS4 + " (15/10)"
      _BOLS = "-"
      _PELS = "link:LS-" + asbh31 + "{ext-relative}[LS zu AS{nbsp}-{nbsp}3.1]"
      _MaLS = "link:LS-" + asma4 + "{ext-relative}[LS zu AS{nbsp}-{nbsp}4]"
      _EnLS = "link:LS-" + asen4 + "{ext-relative}[LS zu AS{nbsp}-{nbsp}4]"
      _WiLS = "link:LS-" + aswi1 + "{ext-relative}[LS zu AS{nbsp}-{nbsp}1]"
      _NaLS = "-"
      _DeLS = "link:LS-" + asde3 + "{ext-relative}[LS zu AS{nbsp}-{nbsp}3]"
      _ReLS = "link:LS-" + asre4 + "{ext-relative}[LS zu AS{nbsp}-{nbsp}4]"
      _SpLS = "link:LS-" + assp4 + "{ext-relative}[LS zu AS{nbsp}-{nbsp}4]"
      _PoLS = "link:LS-" + aspo4 + "{ext-relative}[LS zu AS{nbsp}-{nbsp}4]"
      array_week_AS[i] = ["begin AS in week " +i.to_s,_BOAS,_PEAS,_MaAS,_EnAS,_WiAS,_NaAS,_DeAS,_ReAS,_SpAS,_PoAS,"end AS in week " +i.to_s]
      array_week_LS[i] = ["begin LS in week " +i.to_s,_BOLS,_PELS,_MaLS,_EnLS,_WiLS,_NaLS,_DeLS,_ReLS,_SpLS,_PoLS,"end LS in week " +i.to_s]

    when 27
      i=i+1
      _BOAS = "-"
      _PEAS = _BHAS31 + " (21/15)"
      _MaAS = _MaAS4 + " (28/14)"
      _EnAS = _EnAS4 + " (20/20)"
      _WiAS = _WiAS1 + " (15/2)"
      _NaAS = "-"
      _DeAS = _DeAS3 + " (20/4)"
      _ReAS = _ReAS4 + " (7/7)"
      _SpAS = _SpAS3 + " (8/1)"
      _PoAS = _PoAS4 + " (15/11)"
      _BOLS = "-"
      _PELS = "link:LS-" + asbh31 + "{ext-relative}[LS zu AS{nbsp}-{nbsp}3.1]"
      _MaLS = "link:LS-" + asma4 + "{ext-relative}[LS zu AS{nbsp}-{nbsp}4]"
      _EnLS = "link:LS-" + asen4 + "{ext-relative}[LS zu AS{nbsp}-{nbsp}4]"
      _WiLS = "link:LS-" + aswi1 + "{ext-relative}[LS zu AS{nbsp}-{nbsp}1]"
      _NaLS = "-"
      _DeLS = "link:LS-" + asde3 + "{ext-relative}[LS zu AS{nbsp}-{nbsp}3]"
      _ReLS = "link:LS-" + asre4 + "{ext-relative}[LS zu AS{nbsp}-{nbsp}4]"
      _SpLS = "link:LS-" + assp3 + "{ext-relative}[LS zu AS{nbsp}-{nbsp}3]"
      _PoLS = "link:LS-" + aspo4 + "{ext-relative}[LS zu AS{nbsp}-{nbsp}4]"
      array_week_AS[i] = ["begin AS in week " +i.to_s,_BOAS,_PEAS,_MaAS,_EnAS,_WiAS,_NaAS,_DeAS,_ReAS,_SpAS,_PoAS,"end AS in week " +i.to_s]
      array_week_LS[i] = ["begin LS in week " +i.to_s,_BOLS,_PELS,_MaLS,_EnLS,_WiLS,_NaLS,_DeLS,_ReLS,_SpLS,_PoLS,"end LS in week " +i.to_s]

    when 28
      i=i+1
      _BOAS = "-"
      _PEAS = _BHAS31 + " (21/18)"
      _MaAS = _MaAS4 + " (28/16)"
      _EnAS = _EnAS5 + " (8/2)"
      _WiAS = _WiAS1 + " (15/3)"
      _NaAS = "-"
      _DeAS = _DeAS3 + " (20/6)"
      _ReAS = "(ER{nbsp}" + _ReAS6 + "){nbsp}(7/1), (KR{nbsp}" + _ReAS5 + "){nbsp}(6/1)"
      _SpAS = _SpAS3 + " (8/2)"
      _PoAS = _PoAS4 + " (15/12)"
      _BOLS = "-"
      _PELS = "link:LS-" + asbh31 + "{ext-relative}[LS zu AS{nbsp}-{nbsp}3.1]"
      _MaLS = "link:LS-" + asma4 + "{ext-relative}[LS zu AS{nbsp}-{nbsp}4]"
      _EnLS = "link:LS-" + asen5 + "{ext-relative}[LS zu AS{nbsp}-{nbsp}5]"
      _WiLS = "link:LS-" + aswi1 + "{ext-relative}[LS zu AS{nbsp}-{nbsp}1]"
      _NaLS = "-"
      _DeLS = "link:LS-" + asde3 + "{ext-relative}[LS zu AS{nbsp}-{nbsp}3]"
      _ReLS = "(ER{nbsp}link:LS-" + asre6 + "{ext-relative}[LS zu AS{nbsp}-{nbsp}6]), (KR{nbsp}link:LS-" + asre5 + "{ext-relative}[LS zu AS{nbsp}-{nbsp}5])"
      _SpLS = "link:LS-" + assp3 + "{ext-relative}[LS zu AS{nbsp}-{nbsp}3]"
      _PoLS = "link:LS-" + aspo4 + "{ext-relative}[LS zu AS{nbsp}-{nbsp}4]"
      array_week_AS[i] = ["begin AS in week " +i.to_s,_BOAS,_PEAS,_MaAS,_EnAS,_WiAS,_NaAS,_DeAS,_ReAS,_SpAS,_PoAS,"end AS in week " +i.to_s]
      array_week_LS[i] = ["begin LS in week " +i.to_s,_BOLS,_PELS,_MaLS,_EnLS,_WiLS,_NaLS,_DeLS,_ReLS,_SpLS,_PoLS,"end LS in week " +i.to_s]

    when 29
      i=i+1
      _BOAS = "-"
      _PEAS = _BHAS31 + " (21/21)"
      _MaAS = _MaAS4 + " (28/18)"
      _EnAS = _EnAS5 + " (8/4)"
      _WiAS = _WiAS1 + " (15/4)"
      _NaAS = "-"
      _DeAS = _DeAS3 + " (20/8)"
      _ReAS = "(ER{nbsp}" + _ReAS6 + "){nbsp}(7/2), (KR{nbsp}" + _ReAS5 + "){nbsp}(6/2)"
      _SpAS = _SpAS3 + " (8/3)"
      _PoAS = _PoAS4 + " (15/13)"
      _BOLS = "-"
      _PELS = "link:LS-" + asbh31 + "{ext-relative}[LS zu AS{nbsp}-{nbsp}3.1]"
      _MaLS = "link:LS-" + asma4 + "{ext-relative}[LS zu AS{nbsp}-{nbsp}4]"
      _EnLS = "link:LS-" + asen5 + "{ext-relative}[LS zu AS{nbsp}-{nbsp}5]"
      _WiLS = "link:LS-" + aswi1 + "{ext-relative}[LS zu AS{nbsp}-{nbsp}1]"
      _NaLS = "-"
      _DeLS = "link:LS-" + asde3 + "{ext-relative}[LS zu AS{nbsp}-{nbsp}3]"
      _ReLS = "(ER{nbsp}link:LS-" + asre6 + "{ext-relative}[LS zu AS{nbsp}-{nbsp}6]), (KR{nbsp}link:LS-" + asre5 + "{ext-relative}[LS zu AS{nbsp}-{nbsp}5])"
      _SpLS = "link:LS-" + assp3 + "{ext-relative}[LS zu AS{nbsp}-{nbsp}3]"
      _PoLS = "link:LS-" + aspo4 + "{ext-relative}[LS zu AS{nbsp}-{nbsp}4]"
      array_week_AS[i] = ["begin AS in week " +i.to_s,_BOAS,_PEAS,_MaAS,_EnAS,_WiAS,_NaAS,_DeAS,_ReAS,_SpAS,_PoAS,"end AS in week " +i.to_s]
      array_week_LS[i] = ["begin LS in week " +i.to_s,_BOLS,_PELS,_MaLS,_EnLS,_WiLS,_NaLS,_DeLS,_ReLS,_SpLS,_PoLS,"end LS in week " +i.to_s]

    when 30
      i=i+1
      _BOAS = "-"
      _PEAS = _BHAS32 + " (9/3)"
      _MaAS = _MaAS4 + " (28/20)"
      _EnAS = _EnAS5 + " (8/6)"
      _WiAS = _WiAS1 + " (15/5)"
      _NaAS = "-"
      _DeAS = _DeAS3 + " (20/10)"
      _ReAS = "(ER{nbsp}" + _ReAS6 + "){nbsp}(7/3), (KR{nbsp}" + _ReAS5 + "){nbsp}(6/3)"
      _SpAS = _SpAS3 + " (8/4)"
      _PoAS = _PoAS4 + " (15/14)"
      _BOLS = "-"
      _PELS = "link:LS-" + asbh32 + "{ext-relative}[LS zu AS{nbsp}-{nbsp}3.2]"
      _MaLS = "link:LS-" + asma4 + "{ext-relative}[LS zu AS{nbsp}-{nbsp}4]"
      _EnLS = "link:LS-" + asen5 + "{ext-relative}[LS zu AS{nbsp}-{nbsp}5]"
      _WiLS = "link:LS-" + aswi1 + "{ext-relative}[LS zu AS{nbsp}-{nbsp}1]"
      _NaLS = "-"
      _DeLS = "link:LS-" + asde3 + "{ext-relative}[LS zu AS{nbsp}-{nbsp}3]"
      _ReLS = "(ER{nbsp}link:LS-" + asre6 + "{ext-relative}[LS zu AS{nbsp}-{nbsp}6]), (KR{nbsp}link:LS-" + asre5 + "{ext-relative}[LS zu AS{nbsp}-{nbsp}5])"
      _SpLS = "link:LS-" + assp3 + "{ext-relative}[LS zu AS{nbsp}-{nbsp}3]"
      _PoLS = "link:LS-" + aspo4 + "{ext-relative}[LS zu AS{nbsp}-{nbsp}4]"
      array_week_AS[i] = ["begin AS in week " +i.to_s,_BOAS,_PEAS,_MaAS,_EnAS,_WiAS,_NaAS,_DeAS,_ReAS,_SpAS,_PoAS,"end AS in week " +i.to_s]
      array_week_LS[i] = ["begin LS in week " +i.to_s,_BOLS,_PELS,_MaLS,_EnLS,_WiLS,_NaLS,_DeLS,_ReLS,_SpLS,_PoLS,"end LS in week " +i.to_s]

    when 31
      i=i+1
      _BOAS = "-"
      _PEAS = _BHAS32 + " (9/6)"
      _MaAS = _MaAS4 + " (28/22)"
      _EnAS = _EnAS5 + " (8/8)"
      _WiAS = _WiAS1 + " (15/6)"
      _NaAS = "-"
      _DeAS = _DeAS3 + " (20/12)"
      _ReAS = "(ER{nbsp}" + _ReAS6 + "){nbsp}(7/4), (KR{nbsp}" + _ReAS5 + "){nbsp}(6/4)"
      _SpAS = _SpAS3 + " (8/5)"
      _PoAS = _PoAS4 + " (15/15)"
      _BOLS = "-"
      _PELS = "link:LS-" + asbh32 + "{ext-relative}[LS zu AS{nbsp}-{nbsp}3.2]"
      _MaLS = "link:LS-" + asma4 + "{ext-relative}[LS zu AS{nbsp}-{nbsp}4]"
      _EnLS = "link:LS-" + asen5 + "{ext-relative}[LS zu AS{nbsp}-{nbsp}5]"
      _WiLS = "link:LS-" + aswi1 + "{ext-relative}[LS zu AS{nbsp}-{nbsp}1]"
      _NaLS = "-"
      _DeLS = "link:LS-" + asde3 + "{ext-relative}[LS zu AS{nbsp}-{nbsp}3]"
      _ReLS = "(ER{nbsp}link:LS-" + asre6 + "{ext-relative}[LS zu AS{nbsp}-{nbsp}6]), (KR{nbsp}link:LS-" + asre5 + "{ext-relative}[LS zu AS{nbsp}-{nbsp}5])"
      _SpLS = "link:LS-" + assp3 + "{ext-relative}[LS zu AS{nbsp}-{nbsp}3]"
      _PoLS = "link:LS-" + aspo4 + "{ext-relative}[LS zu AS{nbsp}-{nbsp}4]"
      array_week_AS[i] = ["begin AS in week " +i.to_s,_BOAS,_PEAS,_MaAS,_EnAS,_WiAS,_NaAS,_DeAS,_ReAS,_SpAS,_PoAS,"end AS in week " +i.to_s]
      array_week_LS[i] = ["begin LS in week " +i.to_s,_BOLS,_PELS,_MaLS,_EnLS,_WiLS,_NaLS,_DeLS,_ReLS,_SpLS,_PoLS,"end LS in week " +i.to_s]

    when 32
      i=i+1
      _BOAS = "-"
      _PEAS = _BHAS32 + " (9/9)"
      _MaAS = _MaAS4 + " (28/24)"
      _EnAS = _EnAS6 + " (12/2)"
      _WiAS = _WiAS1 + " (15/7)"
      _DeAS = _DeAS3 + " (20/14)"
      _ReAS = "(ER{nbsp}" + _ReAS6 + "){nbsp}(7/5), (KR{nbsp}" + _ReAS5 + "){nbsp}(6/5)"
      _SpAS = _SpAS3 + " (8/6)"
      _PoAS = _PoAS3 + " (9/1)"
      _BOLS = "-"
      _PELS = "link:LS-" + asbh32 + "{ext-relative}[LS zu AS{nbsp}-{nbsp}3.2]"
      _MaLS = "link:LS-" + asma4 + "{ext-relative}[LS zu AS{nbsp}-{nbsp}4]"
      _EnLS = "link:LS-" + asen6 + "{ext-relative}[LS zu AS{nbsp}-{nbsp}6]"
      _WiLS = "link:LS-" + aswi1 + "{ext-relative}[LS zu AS{nbsp}-{nbsp}1]"
      _NaLS = "-"
      _DeLS = "link:LS-" + asde3 + "{ext-relative}[LS zu AS{nbsp}-{nbsp}3]"
      _ReLS = "(ER{nbsp}link:LS-" + asre6 + "{ext-relative}[LS zu AS{nbsp}-{nbsp}6]), (KR{nbsp}link:LS-" + asre5 + "{ext-relative}[LS zu AS{nbsp}-{nbsp}5])"
      _SpLS = "link:LS-" + assp3 + "{ext-relative}[LS zu AS{nbsp}-{nbsp}3]"
      _PoLS = "link:LS-" + aspo3 + "{ext-relative}[LS zu AS{nbsp}-{nbsp}3]"
      array_week_AS[i] = ["begin AS in week " +i.to_s,_BOAS,_PEAS,_MaAS,_EnAS,_WiAS,_NaAS,_DeAS,_ReAS,_SpAS,_PoAS,"end AS in week " +i.to_s]
      array_week_LS[i] = ["begin LS in week " +i.to_s,_BOLS,_PELS,_MaLS,_EnLS,_WiLS,_NaLS,_DeLS,_ReLS,_SpLS,_PoLS,"end LS in week " +i.to_s]

    when 33
      i=i+1
      _BOAS = "-"
      _PEAS = _BHAS41 + " (9/3)"
      _MaAS = _MaAS4 + " (28/26)"
      _EnAS = _EnAS6 + " (12/4)"
      _WiAS = _WiAS1 + " (15/8)"
      _NaAS = "-"
      _DeAS = _DeAS3 + " (20/16)"
      _ReAS = "(ER{nbsp}" + _ReAS6 + "){nbsp}(7/6), (KR{nbsp}" + _ReAS5 + "){nbsp}(6/6)"
      _SpAS = _SpAS3 + " (8/7)"
      _PoAS = _PoAS3 + " (9/2)"
      _BOLS = "-"
      _PELS = "link:LS-" + asbh41 + "{ext-relative}[LS zu AS{nbsp}-{nbsp}4.1]"
      _MaLS = "link:LS-" + asma4 + "{ext-relative}[LS zu AS{nbsp}-{nbsp}4]"
      _EnLS = "link:LS-" + asen6 + "{ext-relative}[LS zu AS{nbsp}-{nbsp}6]"
      _WiLS = "link:LS-" + aswi1 + "{ext-relative}[LS zu AS{nbsp}-{nbsp}1]"
      _NaLS = "-"
      _DeLS = "link:LS-" + asde3 + "{ext-relative}[LS zu AS{nbsp}-{nbsp}3]"
      _ReLS = "(ER{nbsp}link:LS-" + asre6 + "{ext-relative}[LS zu AS{nbsp}-{nbsp}6]), (KR{nbsp}link:LS-" + asre5 + "{ext-relative}[LS zu AS{nbsp}-{nbsp}5])"
      _SpLS = "link:LS-" + assp3 + "{ext-relative}[LS zu AS{nbsp}-{nbsp}3]"
      _PoLS = "link:LS-" + aspo3 + "{ext-relative}[LS zu AS{nbsp}-{nbsp}3]"
      array_week_AS[i] = ["begin AS in week " +i.to_s,_BOAS,_PEAS,_MaAS,_EnAS,_WiAS,_NaAS,_DeAS,_ReAS,_SpAS,_PoAS,"end AS in week " +i.to_s]
      array_week_LS[i] = ["begin LS in week " +i.to_s,_BOLS,_PELS,_MaLS,_EnLS,_WiLS,_NaLS,_DeLS,_ReLS,_SpLS,_PoLS,"end LS in week " +i.to_s]

    when 34
      i=i+1
      _BOAS = "-"
      _PEAS = _BHAS41 + " (9/6)"
      _MaAS = _MaAS4 + " (28/28)"
      _EnAS = _EnAS6 + " (12/6)"
      _WiAS = _WiAS1 + " (15/9)"
      _NaAS = "-"
      _DeAS = _DeAS3 + " (20/18)"
      _ReAS = "(ER{nbsp}" + _ReAS6 + "){nbsp}(7/7), (KR{nbsp}" + _ReAS6 + "){nbsp}(7/1)"
      _SpAS = _SpAS3 + " (8/8)"
      _PoAS = _PoAS3 + " (9/3)"
      _BOLS = "-"
      _PELS = "link:LS-" + asbh41 + "{ext-relative}[LS zu AS{nbsp}-{nbsp}4.1]"
      _MaLS = "link:LS-" + asma4 + "{ext-relative}[LS zu AS{nbsp}-{nbsp}4]"
      _EnLS = "link:LS-" + asen6 + "{ext-relative}[LS zu AS{nbsp}-{nbsp}6]"
      _WiLS = "link:LS-" + aswi1 + "{ext-relative}[LS zu AS{nbsp}-{nbsp}1]"
      _NaLS = "-"
      _DeLS = "link:LS-" + asde3 + "{ext-relative}[LS zu AS{nbsp}-{nbsp}3]"
      _ReLS = "(ER{nbsp}link:LS-" + asre6 + "{ext-relative}[LS zu AS{nbsp}-{nbsp}6]), (KR{nbsp}link:LS-" + asre6 + "{ext-relative}[LS zu AS{nbsp}-{nbsp}6])"
      _SpLS = "link:LS-" + assp3 + "{ext-relative}[LS zu AS{nbsp}-{nbsp}3]"
      _PoLS = "link:LS-" + aspo3 + "{ext-relative}[LS zu AS{nbsp}-{nbsp}3]"
      array_week_AS[i] = ["begin AS in week " +i.to_s,_BOAS,_PEAS,_MaAS,_EnAS,_WiAS,_NaAS,_DeAS,_ReAS,_SpAS,_PoAS,"end AS in week " +i.to_s]
      array_week_LS[i] = ["begin LS in week " +i.to_s,_BOLS,_PELS,_MaLS,_EnLS,_WiLS,_NaLS,_DeLS,_ReLS,_SpLS,_PoLS,"end LS in week " +i.to_s]

    when 35
      i=i+1
      _BOAS = "-"
      _PEAS = _BHAS41 + " (9/9)"
      _MaAS = _MaAS2 + " (8/2)"
      _EnAS = _EnAS6 + " (12/8)"
      _WiAS = _WiAS1 + " (15/10)"
      _NaAS = "-"
      _DeAS = _DeAS3 + " (20/20)"
      _ReAS = "(ER{nbsp}" + _ReAS5 + "){nbsp}(6/1), (KR{nbsp}" + _ReAS6 + "){nbsp}(7/2)"
      _SpAS = _SpAS5 + " (6/1)"
      _PoAS = _PoAS3 + " (9/4)"
      _BOLS = "-"
      _PELS = "link:LS-" + asbh41 + "{ext-relative}[LS zu AS{nbsp}-{nbsp}4.1]"
      _MaLS = "link:LS-" + asma2 + "{ext-relative}[LS zu AS{nbsp}-{nbsp}2]"
      _EnLS = "link:LS-" + asen6 + "{ext-relative}[LS zu AS{nbsp}-{nbsp}6]"
      _WiLS = "link:LS-" + aswi1 + "{ext-relative}[LS zu AS{nbsp}-{nbsp}1]"
      _NaLS = "-"
      _DeLS = "link:LS-" + asde3 + "{ext-relative}[LS zu AS{nbsp}-{nbsp}3]"
      _ReLS = "(ER{nbsp}link:LS-" + asre5 + "{ext-relative}[LS zu AS{nbsp}-{nbsp}5]), (KR{nbsp}link:LS-" + asre6 + "{ext-relative}[LS zu AS{nbsp}-{nbsp}6])"
      _SpLS = "link:LS-" + assp5 + "{ext-relative}[LS zu AS{nbsp}-{nbsp}5]"
      _PoLS = "link:LS-" + aspo3 + "{ext-relative}[LS zu AS{nbsp}-{nbsp}3]"
      array_week_AS[i] = ["begin AS in week " +i.to_s,_BOAS,_PEAS,_MaAS,_EnAS,_WiAS,_NaAS,_DeAS,_ReAS,_SpAS,_PoAS,"end AS in week " +i.to_s]
      array_week_LS[i] = ["begin LS in week " +i.to_s,_BOLS,_PELS,_MaLS,_EnLS,_WiLS,_NaLS,_DeLS,_ReLS,_SpLS,_PoLS,"end LS in week " +i.to_s]

    when 36
      i=i+1
      _BOAS = "-"
      _PEAS = _BHAS42 + " (6/3)"
      _MaAS = _MaAS2 + " (8/4)"
      _EnAS = _EnAS6 + " (12/10)"
      _WiAS = _WiAS1 + " (15/11)"
      _NaAS = "-"
      _DeAS = _DeAS4 + " (10/2)"
      _ReAS = "(ER{nbsp}" + _ReAS5 + "){nbsp}(6/2), (KR{nbsp}" + _ReAS6 + "){nbsp}(7/3)"
      _SpAS = _SpAS5 + " (6/2)"
      _PoAS = _PoAS3 + " (9/5)"
      _BOLS = "-"
      _PELS = "link:LS-" + asbh42 + "{ext-relative}[LS zu AS{nbsp}-{nbsp}4.2]"
      _MaLS = "link:LS-" + asma2 + "{ext-relative}[LS zu AS{nbsp}-{nbsp}2]"
      _EnLS = "link:LS-" + asen6 + "{ext-relative}[LS zu AS{nbsp}-{nbsp}6]"
      _WiLS = "link:LS-" + aswi1 + "{ext-relative}[LS zu AS{nbsp}-{nbsp}1]"
      _NaLS = "-"
      _DeLS = "link:LS-" + asde4 + "{ext-relative}[LS zu AS{nbsp}-{nbsp}4]"
      _ReLS = "(ER{nbsp}link:LS-" + asre5 + "{ext-relative}[LS zu AS{nbsp}-{nbsp}5]), (KR{nbsp}link:LS-" + asre6 + "{ext-relative}[LS zu AS{nbsp}-{nbsp}6])"
      _SpLS = "link:LS-" + assp5 + "{ext-relative}[LS zu AS{nbsp}-{nbsp}5]"
      _PoLS = "link:LS-" + aspo3 + "{ext-relative}[LS zu AS{nbsp}-{nbsp}3]"
      array_week_AS[i] = ["begin AS in week " +i.to_s,_BOAS,_PEAS,_MaAS,_EnAS,_WiAS,_NaAS,_DeAS,_ReAS,_SpAS,_PoAS,"end AS in week " +i.to_s]
      array_week_LS[i] = ["begin LS in week " +i.to_s,_BOLS,_PELS,_MaLS,_EnLS,_WiLS,_NaLS,_DeLS,_ReLS,_SpLS,_PoLS,"end LS in week " +i.to_s]

    when 37
      i=i+1
      _BOAS = "-"
      _PEAS = _BHAS42 + " (6/6)"
      _MaAS = _MaAS2 + " (8/6)"
      _EnAS = _EnAS6 + " (12/12)"
      _WiAS = _WiAS1 + " (15/12)"
      _NaAS = "-"
      _DeAS = _DeAS4 + " (10/4)"
      _ReAS = "(ER{nbsp}" + _ReAS5 + "){nbsp}(6/3), (KR{nbsp}" + _ReAS6 + "){nbsp}(7/4)"
      _SpAS = _SpAS5 + " (6/3)"
      _PoAS = _PoAS3 + " (9/6)"
      _BOLS = "-"
      _PELS = "link:LS-" + asbh42 + "{ext-relative}[LS zu AS{nbsp}-{nbsp}4.2]"
      _MaLS = "link:LS-" + asma2 + "{ext-relative}[LS zu AS{nbsp}-{nbsp}2]"
      _EnLS = "link:LS-" + asen6 + "{ext-relative}[LS zu AS{nbsp}-{nbsp}6]"
      _WiLS = "link:LS-" + aswi1 + "{ext-relative}[LS zu AS{nbsp}-{nbsp}1]"
      _NaLS = "-"
      _DeLS = "link:LS-" + asde4 + "{ext-relative}[LS zu AS{nbsp}-{nbsp}4]"
      _ReLS = "(ER{nbsp}link:LS-" + asre5 + "{ext-relative}[LS zu AS{nbsp}-{nbsp}5]), (KR{nbsp}link:LS-" + asre6 + "{ext-relative}[LS zu AS{nbsp}-{nbsp}6])"
      _SpLS = "link:LS-" + assp5 + "{ext-relative}[LS zu AS{nbsp}-{nbsp}5]"
      _PoLS = "link:LS-" + aspo3 + "{ext-relative}[LS zu AS{nbsp}-{nbsp}3]"
      array_week_AS[i] = ["begin AS in week " +i.to_s,_BOAS,_PEAS,_MaAS,_EnAS,_WiAS,_NaAS,_DeAS,_ReAS,_SpAS,_PoAS,"end AS in week " +i.to_s]
      array_week_LS[i] = ["begin LS in week " +i.to_s,_BOLS,_PELS,_MaLS,_EnLS,_WiLS,_NaLS,_DeLS,_ReLS,_SpLS,_PoLS,"end LS in week " +i.to_s]

    when 38
      i=i+1
      _BOAS = _BHAS51 + " (6/3)"
      _PEAS = "-"
      _MaAS = _MaAS2 + " (8/8)"
      _EnAS = _EnAS1 + " (6/2)"
      _WiAS = _WiAS1 + " (15/13)"
      _NaAS = "-"
      _DeAS = _DeAS4 + " (10/6)"
      _ReAS = "(ER{nbsp}" + _ReAS5 + "){nbsp}(6/4), (KR{nbsp}" + _ReAS6 + "){nbsp}(7/5)"
      _SpAS = _SpAS5 + " (6/4)"
      _PoAS = _PoAS3 + " (9/7)"
      _BOLS = "link:LS-" + asbh51 + "{ext-relative}[LS zu AS{nbsp}-{nbsp}5.1]"
      _PELS = "-"
      _MaLS = "link:LS-" + asma2 + "{ext-relative}[LS zu AS{nbsp}-{nbsp}2]"
      _EnLS = "link:LS-" + asen1 + "{ext-relative}[LS zu AS{nbsp}-{nbsp}6]"
      _WiLS = "link:LS-" + aswi1 + "{ext-relative}[LS zu AS{nbsp}-{nbsp}1]"
      _NaLS = "-"
      _DeLS = "link:LS-" + asde4 + "{ext-relative}[LS zu AS{nbsp}-{nbsp}4]"
      _ReLS = "(ER{nbsp}link:LS-" + asre5 + "{ext-relative}[LS zu AS{nbsp}-{nbsp}5]), (KR{nbsp}link:LS-" + asre6 + "{ext-relative}[LS zu AS{nbsp}-{nbsp}6])"
      _SpLS = "link:LS-" + assp5 + "{ext-relative}[LS zu AS{nbsp}-{nbsp}5]"
      _PoLS = "link:LS-" + aspo3 + "{ext-relative}[LS zu AS{nbsp}-{nbsp}3]"
      array_week_AS[i] = ["begin AS in week " +i.to_s,_BOAS,_PEAS,_MaAS,_EnAS,_WiAS,_NaAS,_DeAS,_ReAS,_SpAS,_PoAS,"end AS in week " +i.to_s]
      array_week_LS[i] = ["begin LS in week " +i.to_s,_BOLS,_PELS,_MaLS,_EnLS,_WiLS,_NaLS,_DeLS,_ReLS,_SpLS,_PoLS,"end LS in week " +i.to_s]

    when 39
      i=i+1
      _BOAS = _BHAS51 + " (6/6)"
      _PEAS = "-"
      _MaAS = _MaAS5 + " (4/2)"
      _EnAS = _EnAS1 + " (6/4)"
      _WiAS = _WiAS1 + " (15/14)"
      _NaAS = "-"
      _DeAS = _DeAS4 + " (10/8)"
      _ReAS = "(ER{nbsp}" + _ReAS5 + "){nbsp}(6/5), (KR{nbsp}" + _ReAS6 + "){nbsp}(7/6)"
      _SpAS = _SpAS5 + " (6/5)"
      _PoAS = _PoAS3 + " (9/8)"
      _BOLS = "link:LS-" + asbh51 + "{ext-relative}[LS zu AS{nbsp}-{nbsp}5.1]"
      _PELS = "-"
      _MaLS = "link:LS-" + asma5 + "{ext-relative}[LS zu AS{nbsp}-{nbsp}5]"
      _EnLS = "link:LS-" + asen1 + "{ext-relative}[LS zu AS{nbsp}-{nbsp}6]"
      _WiLS = "link:LS-" + aswi1 + "{ext-relative}[LS zu AS{nbsp}-{nbsp}1]"
      _NaLS = "-"
      _DeLS = "link:LS-" + asde4 + "{ext-relative}[LS zu AS{nbsp}-{nbsp}4]"
      _ReLS = "(ER{nbsp}link:LS-" + asre5 + "{ext-relative}[LS zu AS{nbsp}-{nbsp}5]), (KR{nbsp}link:LS-" + asre6 + "{ext-relative}[LS zu AS{nbsp}-{nbsp}6])"
      _SpLS = "link:LS-" + assp5 + "{ext-relative}[LS zu AS{nbsp}-{nbsp}5]"
      _PoLS = "link:LS-" + aspo3 + "{ext-relative}[LS zu AS{nbsp}-{nbsp}3]"
      array_week_AS[i] = ["begin AS in week " +i.to_s,_BOAS,_PEAS,_MaAS,_EnAS,_WiAS,_NaAS,_DeAS,_ReAS,_SpAS,_PoAS,"end AS in week " +i.to_s]
      array_week_LS[i] = ["begin LS in week " +i.to_s,_BOLS,_PELS,_MaLS,_EnLS,_WiLS,_NaLS,_DeLS,_ReLS,_SpLS,_PoLS,"end LS in week " +i.to_s]

    when 40
      i=i+1
      _BOAS = _BHAS61 + " (3/3)"
      _PEAS = "-"
      _MaAS = _MaAS5 + " (4/4)"
      _EnAS = _EnAS1 + " (6/6)"
      _WiAS = _WiAS1 + " (15/15)"
      _NaAS = "-"
      _DeAS = _DeAS4 + " (10/10)"
      _ReAS = "(ER{nbsp}" + _ReAS5 + "){nbsp}(6/6), (KR{nbsp}" + _ReAS6 + "){nbsp}(7/7)"
      _SpAS = _SpAS5 + " (6/6)"
      _PoAS = _PoAS3 + " (9/9)"
      _BOLS = "link:LS-" + asbh61 + "{ext-relative}[LS zu AS{nbsp}-{nbsp}6.1]"
      _PELS = "-"
      _MaLS = "link:LS-" + asma5 + "{ext-relative}[LS zu AS{nbsp}-{nbsp}5]"
      _EnLS = "link:LS-" + asen1 + "{ext-relative}[LS zu AS{nbsp}-{nbsp}6]"
      _WiLS = "link:LS-" + aswi1 + "{ext-relative}[LS zu AS{nbsp}-{nbsp}1]"
      _NaLS = "-"
      _DeLS = "link:LS-" + asde4 + "{ext-relative}[LS zu AS{nbsp}-{nbsp}4]"
      _ReLS = "(ER{nbsp}link:LS-" + asre5 + "{ext-relative}[LS zu AS{nbsp}-{nbsp}5]), (KR{nbsp}link:LS-" + asre6 + "{ext-relative}[LS zu AS{nbsp}-{nbsp}6])"
      _SpLS = "link:LS-" + assp5 + "{ext-relative}[LS zu AS{nbsp}-{nbsp}5]"
      _PoLS = "link:LS-" + aspo3 + "{ext-relative}[LS zu AS{nbsp}-{nbsp}3]"
      array_week_AS[i] = ["begin AS in week " +i.to_s,_BOAS,_PEAS,_MaAS,_EnAS,_WiAS,_NaAS,_DeAS,_ReAS,_SpAS,_PoAS,"end AS in week " +i.to_s]
      array_week_LS[i] = ["begin LS in week " +i.to_s,_BOLS,_PELS,_MaLS,_EnLS,_WiLS,_NaLS,_DeLS,_ReLS,_SpLS,_PoLS,"end LS in week " +i.to_s]
  end #end situations in week 1 to 40

new = "tmp/" + bfbh + "-" + wpt + "-%02d" % m.to_s + ".asciidoc"
f = File.new(new, "w")
f.write(":ext-relative: {outfilesuffix}\n")
f.write(":nofooter:\n")
f.write(":notitle:\n")
f.write(":doctype: article \n")
f.write(":doctitle: Wochenplan Woche " + "%02d" % m + " \n")
f.write(":authors: Burgsmüller, Leslie; Boukari, Murthala; Brentano, Cathrin;")
f.write(" Erdelkamp, Arno; Faget, Britta; Friese; Alexander; Imig, Estelle; Kemper, Katharina;")
f.write(" Loos, Andreas; Peñaloza Ramírez, Rubén; Reschke, Norbert; Schumacher, Julia;")
f.write(" Sloot, Ernst-Otto; Stachel, Thomas; Vogt, Catharina; Waniek, Martin; Witthaus Victoria \n")
f.write(":subject: Wochenplan Woche " + "%02d" % m + " \n")
f.write(":keywords: Wochenplan, Hans-Sachs-Berufskolleg, APO-BK, Didaktische Jahresplanung, Ausbildungsvorbereitung, Nordrhein-Westfalen, Fachbereich Technik, Berufsfeld Bau- und Holztechnik \n")
f.write(":copyright: CC-BY-SA 4.0 \n")
f.write(":email: Norbert.Reschke@gMail.com \n")
f.write("\n")
f.write("== Wochenplan nach link:" + av1 + "{ext-relative}[APO-BK Anlage A 2.1] \n")
f.write("[.center,cols=\"<3,^2,^2,^2,^2\"] \n")
f.write("|=== \n")
f.write("5+^|Ausbildungsvorbereitung in Teilzeitform, *Woche " + m.to_s + "* \n")
f.write("|Lernbereiche und Fächer               |An&shy;for&shy;de&shy;rungs&shy;si&shy;tu&shy;a&shy;tion [small]#(Ge&shy;samt&shy;stun&shy;den / Stun&shy;den in Wo&shy;che{nbsp}" +m.to_s+")# |zu&shy;ge&shy;ord&shy;nete Lern&shy;si&shy;tu&shy;a&shy;tion&shy;en |Unter&shy;richt<<note1,^1)^>> in Wo&shy;che{nbsp}"+m.to_s+" +\n [small]#[Stun&shy;den]#|Unter&shy;richt Wo&shy;che{nbsp}1 bis Ende der Wo&shy;che{nbsp}"+m.to_s+" +\n [small]#[Stun&shy;den]# \n")
f.write(" \n")
f.write("|*Berufsbezogener Lernbereich*         | |      |*" + _BeBe.to_s + "*|*" + (_BeBe*m).to_s + "*\n")
f.write("|[smallyellow]#Fächer des Fach&shy;bereichs#| |      |[smallyellow]#(" + _FaFa.to_s + ")#|[smallyellow]#(" + (_FaFa*m).to_s + ")# \n")
f.write("|" + _l_BO + "|" + _BOAS + "|" + _BOLS + "|" + _BO.to_s + "|" + sBO.to_s + "\n")
f.write("|" + _l_PE + "|" + _PEAS + "|" + _PELS + "|" + _PE.to_s + "|" + sPE.to_s + "\n")
f.write("|" + _l_Ma + "|" + _MaAS + "|" + _MaLS + "|" + _Ma.to_s + "|" + (_Ma*m).to_s + "\n")
f.write("|" + _l_En + "|" + _EnAS + "|" + _EnLS + "|" + _En.to_s + "|" + (_En*m).to_s + "\n")
f.write("|" + _l_Wi + "|" + _WiAS + "|" + _WiLS + "|" + _Wi.to_s + "|" + (_Wi*m).to_s + "\n")
f.write("|" + _l_Na + "|" + _NaAS + "|" + _NaLS + "|" + _Na.to_s + "|" + (_Na*m).to_s + "\n")
f.write(" \n")
f.write("|*Berufsübergreifender Lernbereich*    | |      |*" + _BeAl.to_s + "*|*" + (_BeAl*m).to_s + "*\n")
f.write("|" + _l_De + "|" + _DeAS + "|" + _DeLS + "|" + _De.to_s + "|" + (_De*m).to_s + "\n")
f.write("|" + _l_Re + "|" + _ReAS + "|" + _ReLS + "|" + _Re.to_s + "|" + (_Re*m).to_s + "\n")
f.write("|" + _l_Sp + "|" + _SpAS + "|" + _SpLS + "|" + _Sp.to_s + "|" + (_Sp*m).to_s + "\n")
f.write("|" + _l_Po + "|" + _PoAS + "|" + _PoLS + "|" + _Po.to_s + "|" + (_Po*m).to_s + "\n")
f.write(" \n")
f.write("|*Differenzierungsbereich*             | |   |*" + _BeDi.to_s + "*|*" + (_BeDi*m).to_s + "*\n")
f.write("|Deutsch      |#nach individuellen Bedürfnissen der Lerner#| |" + _Fu.to_s + "|" + (_Fu*m).to_s + "\n")
f.write(" \n")
f.write("|Gesamtstundenzahl                     | | |*" + _SuSu.to_s + "*|" + _SuWo.to_s + " \n")
f.write("|=== \n")
f.write(" \n")
f.write("[[note1]] ^1)^ Unterricht an 2 Tagen und mit mindestens 12 Stunden pro Woche, oder *14 Stunden* mit Ziel *Hauptschulabschluss gleichwertiger Abschluss* \n")
f.write(" \n")
f.write("[[note2]] ^2)^ *mindestens 2 Stunden* pro Woche in diesem Fach, als weitere Bedingung zur Qualifikation *Hauptschulabschluss gleichwertiger Abschluss* \n")
f.write(" \n")
f.write("[[note3]] ^3)^ im Bildungsplan aufgeführte Deskriptoren zur Leistungsbewertung sind obsolet, verbindliche Daten liegen hier: \n")
f.write("link:https://www.berufsbildung.nrw.de/cms/upload/_lehrplaene/deskriptorenliste_fremdsprachen__leistungsbewertung_a2.pdf[Deskriptoren zur Leistungsbewertung] \n")
f.write(" \n")
f.write("[[note4]] ^4)^ link:https://www.berufsbildung.nrw.de/cms/upload/ausbildungsvorbereitung/technik_naturwissenschaft/av_technik_kath-rel.pdf[Katholische Religionslehre]\n")
f.write(" \n")
f.write("[[note5]] ^5)^ link:https://www.berufsbildung.nrw.de/cms/upload/ausbildungsvorbereitung/technik_naturwissenschaft/av_technik_ev-rel.pdf[Evangelische Religionslehre]\n")
f.write(" \n")
f.write("[[note6]] ^6)^ link:https://www.berufsbildung.nrw.de/cms/upload/ausbildungsvorbereitung/technik_naturwissenschaft/av_technik_islamische-religion.pdf[Islamische Religionslehre] und ein link:https://www.berufsbildung.nrw.de/cms/bildungsganguebergreifende-themen/glossar-iru/glossar-iru.html[Glossar arabischer Begriffe]\n")
f.write(" \n")
f.write("[[note7]] ^7)^ link:https://www.berufsbildung.nrw.de/cms/upload/ausbildungsvorbereitung/technik_naturwissenschaft/av_technik_pph.pdf[Praktische Philosophie]\n")
f.write(" \n")
f.write("image:fake.svg[fake image to use fixed buttons] +\n")
f.write(" \n")
f.write("[l1b]#image:home.svg[home,100%,link=index{ext-relative}]# \n")
f.write("[l2b]#image:back.svg[back,100%,link="+ backPage +"{ext-relative}]# \n")
f.write("[l3b]#image:forward.svg[next,100%,link="+ nextPage +"{ext-relative}]# \n")
f.write("[l4b]#image:matrix.svg[matrix,100%," + "link=matrix-02-%02d" %m.to_s + "{ext-relative}]# \n")
f.close

Asciidoctor.render_file new,
:base_dir => '.',
:to_dir => 'site/',
:safe => 'safe',
:attributes => 'linkcss stylesdir=. stylesheet=adoc.css imagesdir=images'
# end making week pages
end # end of weekplans

# all data in array_week_AS , first week is in array_week_AS[1] (arrays first element has index 0, this is the subject)
# in first week the AS for subjects can be found in: 
# Betriebsorganisation, Anforderungssituation: array_week_AS[1][1] 
# Produkterstellung, Anforderungssituation:    array_week_AS[1][2]
# Mathe, Anforderungssituation:                array_week_AS[1][3]
# Englisch, Anforderungssituation              array_week_AS[1][4]
# Wirtschaftslehre, Anforderungssituation      array_week_AS[1][5]
# Naturwissenschaft, Anforderungssituation     array_week_AS[1][6]
# Deutsch, Anforderungssituation               array_week_AS[1][7]
# Religion, Anforderungssituation              array_week_AS[1][8]
# Sport, Anforderungssituation                 array_week_AS[1][9]
# Politik, Anforderungssituation               array_week_AS[1][10]

for r in 1..10
  for s in 1..40
  new = "tmp/matrix-%02d" % r.to_s + "-%02d" % s.to_s + ".asciidoc"
  f = File.new(new, "w")
  f.write(":ext-relative: {outfilesuffix}\n")
  f.write(":notitle:\n")
  f.write(":doctype: article \n")
  f.write(":doctitle: " + array_week_AS[0][r] + "\n")
  f.write(":authors: Burgsmüller, Leslie; Boukari, Murthala; Brentano, Cathrin;")
  f.write(" Erdelkamp, Arno; Faget, Britta; Friese; Alexander; Imig, Estelle; Kemper, Katharina;")
  f.write(" Loos, Andreas; Peñaloza Ramírez, Rubén; Reschke, Norbert; Schumacher, Julia;")
  f.write(" Sloot, Ernst-Otto; Stachel, Thomas; Vogt, Catharina; Waniek, Martin; Witthaus Victoria \n")
  f.write(":subject: Matrix der Lernbereiche,Fächer und Anforderungssituationen abgebildet auf ein Schuljahr mit 40 Wochen \n")
  f.write(":keywords: Wochenmatrix, Hans-Sachs-Berufskolleg, APO-BK, Didaktische Jahresplanung, Ausbildungsvorbereitung, Nordrhein-Westfalen, Fachbereich Technik, Berufsfeld Bau- und Holztechnik \n")
  f.write(":copyright: CC-BY-SA 4.0 \n")
  f.write(":email: Norbert.Reschke@gMail.com \n")
  f.write(":nofooter: \n")
  f.write("\n")

  if array_week_AS[s][r] == "-"
    array_week_AS[s][r] = "keine Situation"
  end
  if array_week_LS[s][r] == "-"
    array_week_LS[s][r] = "keine Situation"
  end

  if s == 40
    s_next = 1
  elsif s < 40
    s_next = s + 1
  end
  if s == 1
    s_back = 40
  elsif s > 1
    s_back = s - 1
  end
  if r == 10
    r_next = 1
  elsif r < 10
    r_next = r + 1
  end
  if r == 1
    r_back = 10
  elsif r > 1
    r_back = r - 1
  end

#  f.write("=== " + array_week_AS[0][r_back] + " +\n")
  f.write("[.flex02] \n")
  f.write("--\n")
  f.write("[.e] \n")
  f.write("Fach\n")
  f.write("\n")
  f.write("[.f]\n")
  f.write(array_week_AS[0][r])
  f.write("\n")
  f.write("[.e] \n")
  f.write("Anforderungs&shy;situation \n")
  f.write("\n")
  f.write("[.f] \n")
  f.write(array_week_AS[s][r])
  f.write("\n")
  f.write("[.e]\n")
  f.write("Lern&shy;situation \n")
  f.write("\n")
  f.write("[.f]\n")
  f.write(array_week_LS[s][r])
  f.write(" \n")
  f.write("--\n")
  f.write("[.flex01] \n")
  f.write("image:w40p" + "%02d" % s.to_s + ".svg[Ausbildungsvorbereitung Woche " + "%02d" % s.to_s + ",350px,align=\"center\",link=" + bfbh + "-" + wpt +"-%02d" % s.to_s + "{outfilesuffix}] \n")
  f.write("\n")
  f.write("[.flex04] \n")
  f.write("image:w40m" + "%02d" % s.to_s + "-%02d" % r.to_s +  ".svg[Ausbildungsvorbereitung Woche " + "%02d" % s.to_s + ",100\%,align=\"center\",link=" + bfbh + "-" + wpt +"-%02d" % s.to_s + "{outfilesuffix}] \n")
  f.write("\n")
  f.write("[top-arrow]#image:matrix-top.svg[arrow-top,42,27,link=matrix-%02d" % r_back.to_s + "-%02d" % s.to_s + "{outfilesuffix}]# \n")
  f.write("[right-arrow]#image:matrix-right.svg[arrow-right,28,42,link=matrix-%02d" % r.to_s + "-%02d" % s_next.to_s + "{outfilesuffix}]# \n")
  f.write("[bottom-arrow]#image:matrix-bottom.svg[arrow-bottom,42,27,link=matrix-%02d" % r_next.to_s + "-%02d" % s.to_s + "{outfilesuffix}]# \n")
  f.write("[left-arrow]#image:matrix-left.svg[arrow-left,28,42,link=matrix-%02d" % r.to_s + "-%02d" % s_back.to_s + "{outfilesuffix}]# \n")
  f.write("[l1b]#image:home.svg[home,100%,link=index{ext-relative}]# \n")
  f.close
  Asciidoctor.render_file new,
  :base_dir => '.',
  :to_dir => 'site/',
  :safe => 'safe',
  :attributes => 'linkcss stylesdir=. stylesheet=adoc.css imagesdir=images lang=de'
  end
end
# end making matrixpage
# begin  making license page
new = "tmp/license.asciidoc"
f = File.new(new, "w")
f.write(":ext-relative: {outfilesuffix}\n")
f.write(":nofooter:\n")
f.write(":notitle:\n")
f.write(":doctype: article \n")
f.write(":doctitle: License \n")
f.write(":authors: Reschke, Norbert \n")
f.write(":subject: Fächer und Anforderungssituationen abgebildet auf ein Schuljahr mit 40 Wochen \n")
f.write(":keywords: Berufskolleg, APO-BK, Didaktische Jahresplanung, Ausbildungsvorbereitung, Nordrhein-Westfalen, Fachbereich Technik, Berufsfeld Bau- und Holztechnik \n")
f.write(":copyright: CC-BY-SA 4.0 \n")
f.write(":email: Norbert.Reschke@gMail.com \n")
f.write("\n")
f.write("include::license.txt[] \n")
f.write(" \n")
f.write("image:fake.svg[fake image to use fixed buttons] +\n")
f.write(" \n")
f.write("[l1b]#image:home.svg[home,100%,link=index{ext-relative}]# \n")
f.write("[l2b]#image:matrix.svg[matrix,100%,link=matrix-02-01{ext-relative}]# \n")
f.close
Asciidoctor.render_file new,
:base_dir => '.',
:to_dir => 'site/',
:safe => 'safe',
:attributes => 'linkcss stylesdir=. stylesheet=adoc.css imagesdir=images lang=de'
# end making license page
