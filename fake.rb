#!/usr/bin/ruby
# encoding: utf-8
require 'fileutils'
require 'rasem'
# https://github.com/aseldawy/rasem

imgname = File.basename(__FILE__,".*")
img = Rasem::SVGImage.new(:width => 150, :height => 1) do
  rectangle(0,0,150,1,:stroke=>"none",:fill=>"none") # background
  end

  FileUtils.mkpath("site/images")
  file = "site/images/" + imgname + ".svg"

  File.open(file, "w") { |f|
    img.write(f)
  }
