== Anforderungssituation 1
Die Absolventinnen und Absolventen beschreiben am Beispiel eines ausgewählten Unternehmens die zugrundeliegende Geschäftsidee.

=== Zielformulierungen
Die Schülerinnen und Schüler beschreiben mit Hilfestellung die _Geschäftsidee_ für ein Unternehmen einer ausgewählten Branche (ZF 1).

Die Schülerinnen und Schüler ermitteln unterschiedliche _Unternehmensziele_ (ZF 2).

Sie beschreiben mögliche _Zielkonflikte_ (ZF 3).

Die Schülerinnen und Schüler beschreiben _Organisationsstrukturen_ von Unternehmen z. B. anhand ihres Praktikumsbetriebs<<note1,^1^>> (ZF 4).


[[note1]] ^1)^ Die Aufbereitung der Organisationsstruktur eignet sich in besonderer Weise als Praktikumsaufgabe.
