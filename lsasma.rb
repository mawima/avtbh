#!/usr/bin/ruby
# encoding: utf-8
require 'fileutils'
require 'rasem'
# https://github.com/aseldawy/rasem

imgname = File.basename(__FILE__,".*").upcase
imgpool = ["1","2","3","4","5"]

lbl1 = "Lernsituation"         # upper small label (label 1)
lbl2 = "LS"                    # big label in short form (label 2)
sbjct = "Mathematik"           # subject 1 or subject part 1
lbl1fs = 10                    # label 1 font-size
lbl2fs = 30                    # label 2 font-size
sbjctfs = 24                   # subject 1 font-size
bckgr = "#e9e9e9"              # image backgroundcolor
bckgrs = "#fff000"             # subject backgroundcolor
rctng = "#e8b1ef"              # backgroundcolor rectangle label 2


imgpool.each { |i|
  img = Rasem::SVGImage.new(:width => 150, :height => 150) do
    rectangle(0,0,150,150,:stroke=>"black",:fill=>bckgr, :stroke_width=>4)
    rectangle(4,34,35,25,:stroke=>"none",:fill=>bckgrs)
    text(75,15,"text-anchor"=>"middle", "dominant-baseline"=>"auto","font-family"=>"sans-serif","font-size"=>lbl1fs,:fill=>"#a0a0a0"){raw lbl1}
    text(75,55,"text-anchor"=>"middle", "dominant-baseline"=>"auto","font-family"=>"sans-serif","font-size"=>sbjctfs,:fill=>"black"){raw sbjct}
    rectangle(15,75,120,60,:stroke=>"black",:fill=>rctng, :stroke_width=>2)
    text(75,115,"text-anchor"=>"middle", "dominant-baseline"=>"auto","font-family"=>"sans-serif","font-size"=>lbl2fs,:fill=>"black"){raw lbl2 + " " + i}
  end

  FileUtils.mkpath("site/images")
  file = "site/images/" + imgname + i + ".svg"

  File.open(file, "w") { |f|
    img.write(f)
    f.write("<!-- Norbert.Reschke@gMail.com, https://creativecommons.org/licenses/by-nc-sa/4.0 -->")
    f.write("\n")
    f.write("<!-- made with rasem: https://github.com/aseldawy/rasem -->")
  }

}
