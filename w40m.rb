
#!/usr/bin/env ruby
# encoding: utf-8
require 'fileutils'
require 'rasem'
# https://hexapdf.gettalong.org
include Math

imgname = File.basename(__FILE__,".*")
white  = '#ffffff'
black  = '#000000'
grey   = '#bccdf7'
green  = '#49db44'
tbh    = '#e8b16f'
grid   = '#a1a1a1'
red    = '#ff0000'

subject= ['BO','PE','M','E','WB','NW','DK','R','SG','PG']

(1..10).each { |j|
  (1..40).each { |i|
    img = Rasem::SVGImage.new(:width => 1232, :height => 332) do
      rectangle(0,0,1232,332,:stroke=>"none",:fill=>white) # background
      (1..10).each { |k|

text(15,k*30+25,"text-anchor"=>"middle","font-family"=>"sans-serif","font-size"=>18,:fill=>black){raw subject[k-1]} # all subjects first col
      }
      rectangle(i*30,0,30,j*30,:stroke=>"none",:fill=>green)     # week (to top)
      rectangle(i*30,j*30+30,30,330,:stroke=>"none",:fill=>green)# week (to bottom)

rectangle(30,j*30,i*30-30,30,:stroke=>"none",:fill=>tbh)#,:opacity=>0.7) # former weeks

rectangle(i*30+30,j*30,1200-i*30,30,:stroke=>"none",:fill=>grey,:opacity=>0.5) # upcoming weeks
      circle(i*30+15,j*30+15,35,:stroke=>"none",:fill=>green)         # subjects
# text(i*30+15,j*30+14,"text-anchor"=>"middle","font-family"=>"sans-serif","font-size"=>15,:fill=>black){raw subject[j-1]} # subject,weeknumber
      line(11*30,0,11*30,330,:stroke=>red,:stroke_width=>4)   # mark 10 weeks
      line(21*30,0,21*30,330,:stroke=>red,:stroke_width=>4)   # mark 20 weeks
      line(31*30,0,31*30,330,:stroke=>red,:stroke_width=>4)   # mark 30 weeks
     (1..41).each { |h|
        line(h*30,30,h*30,330,:stroke=>grid)  # vertical grid

text(h*30+15,25,"text-anchor"=>"middle","font-family"=>"sans-serif","font-size"=>20,:fill=>black){raw h} # weeknumber
      }
      (1..11).each { |g|
        line(30,g*30,1230,g*30,:stroke=>grid) # horizontal grid
      }

# AS Betriebsorganisation

rectangle(30*19,30,30*2,30,:stroke=>black,:fill=>"none",:stroke_width=>2) # Betriebsorganisation AS 1.1

text(30*19+5,30*2-2,"text-anchor"=>"left","font-family"=>"sans-serif","font-size"=>15,:fill=>black){raw "AS 1.1"}

rectangle(30*21,30,30*2,30,:stroke=>black,:fill=>"none",:stroke_width=>2) # Betriebsorganisation AS 1.2

text(30*21+5,30*2-2,"text-anchor"=>"left","font-family"=>"sans-serif","font-size"=>15,:fill=>black){raw "AS 1.2"}

rectangle(30*38,30,30*2,30,:stroke=>black,:fill=>"none",:stroke_width=>2) # Betriebsorganisation AS 5.1

text(30*38+5,30*2-2,"text-anchor"=>"left","font-family"=>"sans-serif","font-size"=>15,:fill=>black){raw "AS 5.1"}

rectangle(30*40,30,30,30,:stroke=>black,:fill=>"none",:stroke_width=>2) # Betriebsorganisation AS 6.1

text(30*40+5,30*2-2,"text-anchor"=>"left","font-family"=>"sans-serif","font-size"=>15,:fill=>black){raw "6.1"}
# AS Produkterstellung

rectangle(30,30*2,30*2,30,:stroke=>black,:fill=>"none",:stroke_width=>2) # Produkterstellung AS 2.1

text(30+5,30*3-2,"text-anchor"=>"left","font-family"=>"sans-serif","font-size"=>15,:fill=>black){raw "AS 2.1"}

rectangle(30*3,30*2,30*6,30,:stroke=>black,:fill=>"none",:stroke_width=>2) # Produkterstellung AS 2.2

text(30*3+10,30*3-2,"text-anchor"=>"left","font-family"=>"sans-serif","font-size"=>15,:fill=>black){raw "PE AS 2.2"}

rectangle(30*9,30*2,30*3,30,:stroke=>black,:fill=>"none",:stroke_width=>2) # Produkterstellung AS 2.3

text(30*9+9,30*3-2,"text-anchor"=>"left","font-family"=>"sans-serif","font-size"=>15,:fill=>black){raw "PE AS 2.3"}

rectangle(30*12,30*2,30*7,30,:stroke=>black,:fill=>"none",:stroke_width=>2) # Produkterstellung AS 2.4

text(30*12+10,30*3-2,"text-anchor"=>"left","font-family"=>"sans-serif","font-size"=>15,:fill=>black){raw "Produkterstellung AS 2.4"}

rectangle(30*23,30*2,30*7,30,:stroke=>black,:fill=>"none",:stroke_width=>2) # Produkterstellung AS 3.1

text(30*23+10,30*3-2,"text-anchor"=>"left","font-family"=>"sans-serif","font-size"=>15,:fill=>black){raw "Produkterstellung AS 3.1"}

rectangle(30*30,30*2,30*3,30,:stroke=>black,:fill=>"none",:stroke_width=>2) # Produkterstellung AS 3.2

text(30*30+9,30*3-2,"text-anchor"=>"left","font-family"=>"sans-serif","font-size"=>15,:fill=>black){raw "PE AS 3.2"}

rectangle(30*33,30*2,30*3,30,:stroke=>black,:fill=>"none",:stroke_width=>2) # Produkterstellung AS 4.1

text(30*33+9,30*3-2,"text-anchor"=>"left","font-family"=>"sans-serif","font-size"=>15,:fill=>black){raw "PE AS 4.1"}

rectangle(30*36,30*2,30*2,30,:stroke=>black,:fill=>"none",:stroke_width=>2) # Produkterstellung AS 4.2

text(30*36+5,30*3-2,"text-anchor"=>"left","font-family"=>"sans-serif","font-size"=>15,:fill=>black){raw "AS 4.2"}
# AS Mathe

rectangle(30,30*3,30*8,30,:stroke=>black,:fill=>"none",:stroke_width=>2) # Mathe AS 1

text(30+10,30*4-2,"text-anchor"=>"left","font-family"=>"sans-serif","font-size"=>15,:fill=>black){raw "Mathematik AS 1"}

rectangle(30*9,30*3,30*12,30,:stroke=>black,:fill=>"none",:stroke_width=>2) # Mathe AS 3

text(30*9+10,30*4-2,"text-anchor"=>"left","font-family"=>"sans-serif","font-size"=>15,:fill=>black){raw "Mathematik AS 3"}

rectangle(30*21,30*3,30*14,30,:stroke=>black,:fill=>"none",:stroke_width=>2) # Mathe AS 4

text(30*21+10,30*4-2,"text-anchor"=>"left","font-family"=>"sans-serif","font-size"=>15,:fill=>black){raw "Mathematik AS 4"}

rectangle(30*35,30*3,30*4,30,:stroke=>black,:fill=>"none",:stroke_width=>2) # Mathe AS 2

text(30*35+10,30*4-2,"text-anchor"=>"left","font-family"=>"sans-serif","font-size"=>15,:fill=>black){raw "Mathe AS 2"}

rectangle(30*39,30*3,30*2,30,:stroke=>black,:fill=>"none",:stroke_width=>2) # Mathe AS 5

text(30*39+10,30*4-2,"text-anchor"=>"left","font-family"=>"sans-serif","font-size"=>15,:fill=>black){raw "AS 5"}
# AS Englisch

rectangle(30,30*4,30*7,30,:stroke=>black,:fill=>"none",:stroke_width=>2) # Englisch AS 3

text(30+10,30*5-2,"text-anchor"=>"left","font-family"=>"sans-serif","font-size"=>15,:fill=>black){raw "Englisch AS 3"}

rectangle(30*8,30*4,30*10,30,:stroke=>black,:fill=>"none",:stroke_width=>2) # Englisch AS 2

text(30*8+10,30*5-2,"text-anchor"=>"left","font-family"=>"sans-serif","font-size"=>15,:fill=>black){raw "Englisch AS 2"}

rectangle(30*18,30*4,30*10,30,:stroke=>black,:fill=>"none",:stroke_width=>2) # Englisch AS 4

text(30*18+10,30*5-2,"text-anchor"=>"left","font-family"=>"sans-serif","font-size"=>15,:fill=>black){raw "Englisch AS 4"}

rectangle(30*28,30*4,30*4,30,:stroke=>black,:fill=>"none",:stroke_width=>2) # Englisch AS 5

text(30*28+10,30*5-2,"text-anchor"=>"left","font-family"=>"sans-serif","font-size"=>15,:fill=>black){raw "Englisch AS 5"}

rectangle(30*32,30*4,30*6,30,:stroke=>black,:fill=>"none",:stroke_width=>2) # Englisch AS 6

text(30*32+10,30*5-2,"text-anchor"=>"left","font-family"=>"sans-serif","font-size"=>15,:fill=>black){raw "Englisch AS 6"}

rectangle(30*38,30*4,30*3,30,:stroke=>black,:fill=>"none",:stroke_width=>2) # Englisch AS 1

text(30*38+30,30*5-2,"text-anchor"=>"left","font-family"=>"sans-serif","font-size"=>15,:fill=>black){raw "AS 1"}
# AS Wirtschafts- und Betriebslehre

rectangle(30,30*5,30*15,30,:stroke=>black,:fill=>"none",:stroke_width=>2) # Wirtschafts- und Betriebslehre AS 2

text(30+10,30*6-2,"text-anchor"=>"left","font-family"=>"sans-serif","font-size"=>15,:fill=>black){raw "Wirtschafts- und Betriebslehre AS 2"}

rectangle(30*16,30*5,30*10,30,:stroke=>black,:fill=>"none",:stroke_width=>2) # Wirtschafts- und Betriebslehre AS 3

text(30*16+10,30*6-2,"text-anchor"=>"left","font-family"=>"sans-serif","font-size"=>15,:fill=>black){raw "Wirtschafts- und Betriebslehre AS 3"}

rectangle(30*26,30*5,30*15,30,:stroke=>black,:fill=>"none",:stroke_width=>2) # Wirtschafts- und Betriebslehre AS 1

text(30*26+10,30*6-2,"text-anchor"=>"left","font-family"=>"sans-serif","font-size"=>15,:fill=>black){raw "Wirtschafts- und Betriebslehre AS 1"}
# AS Deutsch/Kommunikation

rectangle(30,30*7,30*5,30,:stroke=>black,:fill=>"none",:stroke_width=>2) # Deutsch AS 2

text(30+10,30*8-2,"text-anchor"=>"left","font-family"=>"sans-serif","font-size"=>15,:fill=>black){raw "Deutsch AS 2"}

rectangle(30*6,30*7,30*10,30,:stroke=>black,:fill=>"none",:stroke_width=>2) # Deutsch AS 1

text(30*6+10,30*8-2,"text-anchor"=>"left","font-family"=>"sans-serif","font-size"=>15,:fill=>black){raw "Deutsch/Kommunikation AS 1"}

rectangle(30*16,30*7,30*10,30,:stroke=>black,:fill=>"none",:stroke_width=>2) # Deutsch AS 5

text(30*16+10,30*8-2,"text-anchor"=>"left","font-family"=>"sans-serif","font-size"=>15,:fill=>black){raw "Deutsch/Kommunikation AS 5"}

rectangle(30*26,30*7,30*10,30,:stroke=>black,:fill=>"none",:stroke_width=>2) # Deutsch AS 3

text(30*26+10,30*8-2,"text-anchor"=>"left","font-family"=>"sans-serif","font-size"=>15,:fill=>black){raw "Deutsch/Kommunikation AS 3"}

rectangle(30*36,30*7,30*5,30,:stroke=>black,:fill=>"none",:stroke_width=>2) # Deutsch AS 4

text(30*36+10,30*8-2,"text-anchor"=>"left","font-family"=>"sans-serif","font-size"=>15,:fill=>black){raw "Deutsch AS 4"}
# AS Religion

rectangle(30,30*8,30*7,30,:stroke=>black,:fill=>"none",:stroke_width=>2) # Religion AS 3

text(30+10,30*9-2,"text-anchor"=>"left","font-family"=>"sans-serif","font-size"=>15,:fill=>black){raw "Religion ER/KR AS 3"}

rectangle(30*8,30*8,30*7,30,:stroke=>black,:fill=>"none",:stroke_width=>2) # Religion AS 1

text(30*8+10,30*9-2,"text-anchor"=>"left","font-family"=>"sans-serif","font-size"=>15,:fill=>black){raw "Religion ER/KR AS 1"}

rectangle(30*15,30*8,30*6,30,:stroke=>black,:fill=>"none",:stroke_width=>2) # Religion AS 2

text(30*15+10,30*9-2,"text-anchor"=>"left","font-family"=>"sans-serif","font-size"=>15,:fill=>black){raw "Religion ER/KR AS 2"}

rectangle(30*21,30*8,30*7,30,:stroke=>black,:fill=>"none",:stroke_width=>2) # Religion AS 4

text(30*21+10,30*9-2,"text-anchor"=>"left","font-family"=>"sans-serif","font-size"=>15,:fill=>black){raw "Religion ER/KR AS 4"}

rectangle(30*28,30*8,30*7,15,:stroke=>black,:fill=>"none",:stroke_width=>2) # ER AS 6

text(30*28+10,30*8.5-2,"text-anchor"=>"left","font-family"=>"sans-serif","font-size"=>14,:fill=>black){raw "Religion ER AS 6"}

rectangle(30*35,30*8,30*6,15,:stroke=>black,:fill=>"none",:stroke_width=>2) # ER AS 5

text(30*35+10,30*8.5-2,"text-anchor"=>"left","font-family"=>"sans-serif","font-size"=>14,:fill=>black){raw "Religion ER AS 5"}

rectangle(30*28,30*8.5,30*6,15,:stroke=>black,:fill=>"none",:stroke_width=>2) # KR AS 5

text(30*28+10,30*9-2,"text-anchor"=>"left","font-family"=>"sans-serif","font-size"=>14,:fill=>black){raw "Religion KR AS 5"}

rectangle(30*34,30*8.5,30*7,15,:stroke=>black,:fill=>"none",:stroke_width=>2) # KR AS 6

text(30*34+10,30*9-2,"text-anchor"=>"left","font-family"=>"sans-serif","font-size"=>14,:fill=>black){raw "Religion KR AS 6"}
# AS Sport

rectangle(30,30*9,30*8,30,:stroke=>black,:fill=>"none",:stroke_width=>2) # Sport AS 1

text(30+10,30*10-2,"text-anchor"=>"left","font-family"=>"sans-serif","font-size"=>15,:fill=>black){raw "Sport AS 1"}

rectangle(30*9,30*9,30*6,30,:stroke=>black,:fill=>"none",:stroke_width=>2) # Sport AS 2

text(30*9+10,30*10-2,"text-anchor"=>"left","font-family"=>"sans-serif","font-size"=>15,:fill=>black){raw "Sport AS 2"}

rectangle(30*15,30*9,30*6,30,:stroke=>black,:fill=>"none",:stroke_width=>2) # Sport AS 6

text(30*15+10,30*10-2,"text-anchor"=>"left","font-family"=>"sans-serif","font-size"=>15,:fill=>black){raw "Sport AS 6"}

rectangle(30*21,30*9,30*6,30,:stroke=>black,:fill=>"none",:stroke_width=>2) # Sport AS 4

text(30*21+10,30*10-2,"text-anchor"=>"left","font-family"=>"sans-serif","font-size"=>15,:fill=>black){raw "Sport AS 4"}

rectangle(30*27,30*9,30*8,30,:stroke=>black,:fill=>"none",:stroke_width=>2) # Sport AS 3

text(30*27+10,30*10-2,"text-anchor"=>"left","font-family"=>"sans-serif","font-size"=>15,:fill=>black){raw "Sport AS 3"}

rectangle(30*35,30*9,30*6,30,:stroke=>black,:fill=>"none",:stroke_width=>2) # Sport AS 5

text(30*35+10,30*10-2,"text-anchor"=>"left","font-family"=>"sans-serif","font-size"=>15,:fill=>black){raw "Sport AS 5"}
# AS Politik

rectangle(30,30*10,30*8,30,:stroke=>black,:fill=>"none",:stroke_width=>2) # Politik AS 2

text(30+10,30*11-2,"text-anchor"=>"left","font-family"=>"sans-serif","font-size"=>15,:fill=>black){raw "Politik AS 2"}

rectangle(30*9,30*10,30*8,30,:stroke=>black,:fill=>"none",:stroke_width=>2) # Politik AS 1

text(30*9+10,30*11-2,"text-anchor"=>"left","font-family"=>"sans-serif","font-size"=>15,:fill=>black){raw "Politik AS 1"}

rectangle(30*17,30*10,30*15,30,:stroke=>black,:fill=>"none",:stroke_width=>2) # Politik AS 4

text(30*17+10,30*11-2,"text-anchor"=>"left","font-family"=>"sans-serif","font-size"=>15,:fill=>black){raw "Politik AS 4"}

rectangle(30*32,30*10,30*9,30,:stroke=>black,:fill=>"none",:stroke_width=>2) # Politik AS 3

text(30*32+10,30*11-2,"text-anchor"=>"left","font-family"=>"sans-serif","font-size"=>15,:fill=>black){raw "Politik AS 3"}

circle(i*30+15,j*30+15,25,:stroke=>"none",:fill=>white,:opacity=>0.7)       # subjects

text(i*30+15,j*30+14,"text-anchor"=>"middle","font-family"=>"sans-serif","font-size"=>15,:fill=>black){raw subject[j-1]} # subject,weeknumber

    end

  FileUtils.mkpath("site/images")
  file = "site/images/" + imgname + "%02d" % + i.to_s  + "-%02d" % j.to_s + ".svg"

  File.open(file, "w") { |f|
    img.write(f)
    f.write("<!-- Norbert.Reschke@gMail.com, https://creativecommons.org/licenses/by-nc-sa/4.0 -->")
    f.write("\n")
    f.write("<!-- made with rasem: https://github.com/aseldawy/rasem -->")
    }
  }
}
