Bauhelfertätigkeiten beim Bau eines Blockhauses (Rohbau) aus
Fichte/Tanne-Dielen auf vier Einzelfundamenten

=== LS1
Konstruktionszeichnungen sichten,
benötigte Werkzeuge und Verbindungsmittel zuordnen
(3h)

=== LS2
Baustoffe an der Baustelle lagern und anreichen
Werkzeuge und Verbindungsmittel bereitstellen
(3h)

=== LS3
Lagern, transportieren, zuarbeiten
(3h)

