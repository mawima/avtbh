== Anforderungssituation 6 (ER) _Ethisch begründetes Handeln_
Ausgehend von ausgewählten christlichen Wertvorstellungen und evangelischer Positionen zum Umgang mit Forschung und Technik leiten die Absolventinnen und Absolventen einfache Orientierungen für ihr eigenes ethisches Handeln in ihrem Berufs- und Privatleben ab.

Mögliche Anknüpfungspunkte zu beruflichen Handlungsfeldern (HF): HF 3(Erstellung; Einsatz von Werkzeugen, Maschinen und Anlagen); HF 4 (Instandhaltung); HF 1 (Personalmanagement, Materialwirtschaft, Steuerung und Kontrolle von Geschäftsprozessen) HF 5 (Umweltmanagement); HF 6 (Qualitätsmanagement) 

Mögliche theologische Anknüpfungspunkte an die Handlungsfelder: Nachfolge Christi; Rechtfertigungslehre; christliche Ethik; Reich-Gottes-Lehre; Nächstenliebe; Gerechtigkeit

=== Zielformulierungen
Die Schülerinnen und Schüler beschreiben grundlegende Positionen christlicher, insbesondere evangelischer Ethik (ZF 1).

Sie begründen angeleitet an Beispielen eigenes ethisches Handeln im Privat- und Berufsleben und tauschen sich darüber aus (ZF 2).

== Anforderungssituation 6 (KR)
Die Absolventinnen und Absolventen nehmen wahr, dass in einem auf Erfolg und Macht ausgerichteten beruflichen und privaten Umfeld Ungleichheit und Ungerechtigkeit entstehen und das sozialfriedliche Miteinander gefährden. Sie erkennen in der Gottebenbildlichkeit und der Geschöpflichkeit des Menschen eine Quelle unbedingter Anerkennung und Grundlage für die Menschenrechte. Sie entwickeln auf dieser Basis eigene Handlungsmöglichkeiten für ihr unmittelbares Umfeld.

=== Zielformulierungen
Die Schülerinnen und Schüler nehmen die Gottebenbildlichkeit und Geschöpflichkeit des Menschen als grundlegend für vorgegebene Aussagen der Menschenrechte und Regelungen für das menschliche Miteinander wahr. (ZF 1)

Sie entwickeln Orientierungen für den Umgang mit anderen Menschen und der Schöpfung. (ZF 2)

Die Schülerinnen und Schüler informieren sich über ausgewählte Beispiele kirchlichen Engagements vor Ort in den Bereichen Arbeit und soziale Gerechtigkeit (z. B. Tafel, Sozialkaufhaus). (ZF 3)
