== Anforderungssituation 4 _Medien verstehen und nutzen_
Die Absolventinnen und Absolventen kennen und nutzen verschiedene Medien zielgerichtet in beruflichen, öffentlichen und persönlichen Handlungszusammenhängen.

=== Zielformulierungen
Die Schülerinnen und Schüler unterscheiden zwischen _Informations- und Unterhaltungsfunktion von Medien_ (ZF 1) (z.B. Untersuchung von Fachartikeln).

Sie kennen _unterschiedliche Medien und_ schätzen die Wirkung von _medienspezifischen Gestaltungsmitteln_ ein (ZF 2) (z.B. ausgewählte Werbetexte untersuchen).

Sie unterscheiden zwischen _Realität und Virtualität in Medien_ (ZF 3) (z.B. Vermarktungsabsichten erkennen).

Unter Einsatz geeigneter _Suchverfahren_ nutzen sie die _Informationsmöglichkeiten unterschiedlicher Medien_ (ZF 4) (z.B. Anfragen zu technischen Inhalten mit Hilfe verschiedener Suchmaschinen).

Sie setzen Medien zielgerichtet und sachbezogen zur _Präsentation_ ein (ZF 5) (z.B. einfache Diagramme erstellen).

Sie produzieren einfache _Medienbeiträge_ (ZF 6) (z.B. einen Beruf in Form eines Flyers vorstellen).
