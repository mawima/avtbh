#!/usr/bin/ruby
# encoding: utf-8
require 'fileutils'
require 'rasem'
# https://github.com/aseldawy/rasem

lbl1 = "Woche"          # upper small label (label 1)
lbl2 = "Teilzeit"       # lower small label (label 2)
lbl1fs = 16             # label 1 font-size
lbl2fs = 110            # label 2 font-size
bckgr = "#000000"       # image backgroundcolor
#bckgr = "#abcdef"       # image backgroundcolor
#bckgr = "#c1c1c1"       # image backgroundcolor

(1..40).each { |i|
  img = Rasem::SVGImage.new(:width => 150, :height => 150) do
    rectangle(0,0,150,150,:stroke=>"#ffffff",:fill=>bckgr, :stroke_width=>8) # background
    text(75,115,"text-anchor"=>"middle", "dominant-baseline"=>"auto","font-family"=>"sans-serif","font-size"=>lbl2fs,:fill=>"white"){raw i}
    text(75,20,"text-anchor"=>"middle", "dominant-baseline"=>"middle","font-family"=>"sans-serif","font-size"=>lbl1fs,:fill=>"white"){raw lbl1}
    text(75,140,"text-anchor"=>"middle", "dominant-baseline"=>"middle","font-family"=>"sans-serif","font-size"=>lbl1fs,:fill=>"white"){raw lbl2}
    rectangle(9,129,12,12,:stroke=>"none",:fill=>"#808080") # form-picto
  end

  FileUtils.mkpath("site/images")
  file = "site/images/" + "%02d" % + i.to_s + ".svg"

  File.open(file, "w") { |f|
    img.write(f)
    f.write("<!-- Norbert.Reschke@gMail.com, https://creativecommons.org/licenses/by-nc-sa/4.0 -->")
    f.write("\n")
    f.write("<!-- made with rasem: https://github.com/aseldawy/rasem -->")
    }

}
