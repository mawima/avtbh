#!/usr/bin/ruby
# encoding: utf-8
require 'fileutils'
require 'rasem'
# https://github.com/aseldawy/rasem

imgname = File.basename(__FILE__,".*").upcase
imgpool = ["1","2","3","4","5","6"]

lbl1 = "Anforderungssituation" # upper small label (label 1)
lbl2 = "AS"                    # big label in short form (label 2)
sbjct1 = "Religion"            # subject 1 or subject part 1
sbjct2 = "ER oder RK"          # subject 2 or subject part 2
lbl1fs = 10                    # label 1 font-size
lbl2fs = 30                    # label 2 font-size
sbjct1fs = 26                  # subject 1 font-size
sbjct2fs = 16                  # subject 2 font-size
bckgr = "#e9e9e9"              # image backgroundcolor
bckgrs = "#b3ff80"             # subject backgroundcolor
rctng = "#ffffff"              # backgroundcolor rectangle label 2

imgpool.each { |i|
  img = Rasem::SVGImage.new(:width => 150, :height => 150) do
    rectangle(0,0,150,150,:stroke=>"black",:fill=>bckgr, :stroke_width=>4)
    rectangle(23,25,34,30,:stroke=>"none",:fill=>bckgrs)
    text(75,15,"text-anchor"=>"middle", "dominant-baseline"=>"auto","font-family"=>"sans-serif","font-size"=>lbl1fs,:fill=>"#a0a0a0"){raw lbl1}
    text(75,50,"text-anchor"=>"middle", "dominant-baseline"=>"auto","font-family"=>"sans-serif","font-size"=>sbjct1fs,:fill=>"black"){raw sbjct1}
    text(75,70,"text-anchor"=>"middle", "dominant-baseline"=>"auto","font-family"=>"sans-serif","font-size"=>sbjct2fs,:fill=>"black"){raw sbjct2}
    rectangle(15,75,120,60,:stroke=>"black",:fill=>rctng, :stroke_width=>2)
    text(75,115,"text-anchor"=>"middle", "dominant-baseline"=>"auto","font-family"=>"sans-serif","font-size"=>lbl2fs,:fill=>"black"){raw lbl2 + " " + i}
  end

  FileUtils.mkpath("site/images")
  file = "site/images/" + imgname + i + ".svg"

  File.open(file, "w") { |f|
    img.write(f)
    f.write("<!-- Norbert.Reschke@gMail.com, https://creativecommons.org/licenses/by-nc-sa/4.0 -->")
    f.write("\n")
    f.write("<!-- made with rasem: https://github.com/aseldawy/rasem -->")
  }

}
