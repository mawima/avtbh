[cols="<3,^2,^2,^2"]
|===
4+^|Ausbildungsvorbereitung in Vollzeitform
|Lernbereiche und Fächer              |Anforderungssituation     |Unterrichtsstunden in dieser Woche <<note1,^1)^>> |bisherige Unterrichtsstunden
|*Berufsbezogener Lernbereich*        | | [*28 -- 30*]           |
|Fächer des Fachbereichs              | | 21 -- 26               | 
|[smallyellow]#Produkterstellung#     | | [smallyellow]#0 -- 26# |
|[smallyellow]#Betriebsorganisation#  | | [smallyellow]#0--26#   |                                   
|Mathematik <<note2,^2)^>>            | | 1 -- 3                 |
|Englisch <<note2,^2)^>>              | | 1--3                   |
|Wirtschafts- und Betriebslehre       | | 1                      |
|Naturwissenschaft                    | | 0--3                   |

|*Berufsübergreifender Lernbereich*   | | [*4--6*]               |
|Deutsch/Kommunikation                | | 1--3                   |
|Religionslehre                       | | 1                      |
|Sport/Gesundheitsförderung           | | 1                      |
|Politik/Gesellschaftslehre           | | 1                      |

|*Differenzierungsbereich*            | | *0--1*                 |

|Gesamtstundenzahl                    | | *34--36*               |
|===

[[note1]] ^1)^ Der im Berufskolleg vermittelte Unterrichtsanteil muss
pro Woche mindestens 12 Unterrichtsstunden (für den Erwerb eines *dem
Hauptschulabschluss gleichwertigen Abschlusses* *14 Stunden*) umfassen.
Der schulisch vermittelte Anteil wird durch ein *betriebliches Praktikum
bis zu drei Tagen* pro Woche oder durch den Besuch einer *berufsvorbereitenden oder
ähnlichen Bildungsmaßnahme* ergänzt. Das Praktikum kann auch in Blockphasen
bis maximal zwei Wochen absolviert werden. Die Jugendlichen sind während
des Praktikums Schülerinnen und Schüler des Berufskollegs. Das Praktikum
wird von den Lehrkraften intensiv begleitet und ist durch
Klassenbucheintrag zu dokumentieren. Soweit der fachpraktische Anteil am
Lernort Betrieb durch das Praktikum nicht oder nicht in vollem Umfang
möglich ist, ist der entsprechende Anteil durch *fachpraktischen
Unterricht im Berufskolleg* sicherzustellen.

[[note2]] ^2)^ Um einen dem *Hauptschulabschluss gleichwertigen Abschluss*
zu ermöglichen, muss der Unterricht in diesen Fächern mit *mindestens 80
Unterrichtsstunden* erteilt werden.
