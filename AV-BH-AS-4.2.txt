== Anforderungssituation 4.2
Die Absolventinnen und nehmen Schäden und Verschleiß an oder in Gebäuden wahr und ordnen sie unterschiedlichen Schadenskategorien zu.

=== Zielformulierungen
Die Schülerinnen und Schüler können _augenscheinliche Beschädigungen an Bauteilen_ wie Türen, Fenstern, Möbelteilen erfassen. Sie nehmen diese Beschädigungen in _Schadensprotokolle_ auf (ZF{nbsp}1).

Die Schülerinnen und Schüler unterscheiden _Kategorien von Schäden_ nach Abnutzung, Fehlmontage, Fehlhandhabung, unfachliche Materialwahl oder unfachliche Kombination von Material, unvorhergesehene äußere Einwirkung (ZF{nbsp}2).

Die Schülerinnen und Schüler formulieren Vermutungen über die _Ursachen von Schäden_ (ZF{nbsp}3).

+++Beispiele:+++ Wasserschäden in Decken/Wänden, Risse in Wänden, Klemmende Türen, nicht schließende Fenster, Das Schadensprotokoll kann dem Hausmeister oder der Schulleitung präsentiert werden
