#!/usr/bin/ruby
# encoding: utf-8
require 'fileutils'
require 'rasem'
# https://github.com/aseldawy/rasem

imgname = File.basename(__FILE__,".*")
fnts    = 20
white   = '#ffffff'
black   = '#000000'
red     = '#ff0000'
img = Rasem::SVGImage.new(:width => 42, :height => 27) do
#img = Rasem::SVGImage.new(:width => 100, :height => 100) do
  defs {
    group(:id => "arrow") {
      polygon([[0,0],[20,-20],[25,-15],[11,0],[25,15],[20,20]],:stroke=>white,:fill=>red)
      }
    }
  use("arrow").translate(21,27).scale(1,-1).rotate(90)
#  use("arrow").translate(75,60+35).scale(0.5,-0.5).rotate(90)
#  use("arrow").translate(75,60-35).scale(0.5,0.5).rotate(90)
  end
FileUtils.mkpath("site/images")
file = "site/images/" + imgname + ".svg"

File.open(file, "w") { |f|
  img.write(f)
  f.write("<!-- Norbert.Reschke@gMail.com, https://creativecommons.org/licenses/by-nc-sa/4.0 -->")
  f.write("\n")
  f.write("<!-- made with rasem: https://github.com/aseldawy/rasem -->")
  }

