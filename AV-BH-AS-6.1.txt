== Anforderungssituation 6.1
Die Absolventinnen und Absolventen vergleichen angeleitet den Ist-Zustand mit dem Soll-Zustand eines Produktes und benennen Verbesserungsmöglichkeiten.

=== Zielformulierungen
Die Schülerinnen und Schüler füllen eine vorgegebene (oder mit Hilfe selbsterstellte) Checkliste aus. Sie lesen die Maße aus der Zeichnung heraus (Sollzustand) und verwenden einfache Messwerkzeuge zum Messen des Ist-Zustandes (ZF{nbsp}1).

Die Schülerinnen und Schüler _erkennen den Nachbesserungsbedarf_ eines fehlerhaften Werkstückes und können unter Anleitung Korrekturmöglichkeiten aufzeigen (ZF{nbsp}2).

Die Schülerinnen und Schüler bewerten anhand der Checkliste das Werkstück und leiten daraus Verhaltensänderungen für die Erstellung zukünftiger Werkstücke ab (ZF{nbsp}3).

+++Beispiele:+++ Fenster, Türen, Küchenschränke, Stühle, Tische, Containermöbel.
