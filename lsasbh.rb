#!/usr/bin/ruby
# encoding: utf-8
require 'fileutils'
require 'rasem'
# https://github.com/aseldawy/rasem

imgname = File.basename(__FILE__,".*").upcase
imgpool = ["1.1","1.2","2.1","2.2","2.3","2.4","3.1","3.2","4.1","4.2","5.1","6.1"]

lbl1 = "Lernsituation"         # upper small label (label 1)
lbl2 = "LS"                    # big label in short form (label 2)
sbjct1 = "Bautechnik"          # subject 1 or subject part 1
sbjct2 = "Holztechnik"         # subject 2 or subject part 2
lbl1fs = 12                    # label 1 font-size
lbl2fs = 30                    # label 2 font-size
sbjct1fs = 20                  # subject 1 font-size
sbjct2fs = 20                  # subject 2 font-size
bckgr = "#e8b16f"              # image backgroundcolor
rctng = "#e8b1ef"              # backgroundcolor rectangle label 2

imgpool.each { |i|
  img = Rasem::SVGImage.new(:width => 150, :height => 150) do
    rectangle(0,0,150,150,:stroke=>"black",:fill=>bckgr, :stroke_width=>4) # background
    text(75,15,"text-anchor"=>"middle", "dominant-baseline"=>"auto","font-family"=>"sans-serif","font-size"=>lbl1fs,:fill=>"white"){raw lbl1}
    text(75,50,"text-anchor"=>"middle", "dominant-baseline"=>"auto","font-family"=>"sans-serif","font-size"=>sbjct1fs,:fill=>"black"){raw sbjct1}
    text(75,70,"text-anchor"=>"middle", "dominant-baseline"=>"auto","font-family"=>"sans-serif","font-size"=>sbjct2fs,:fill=>"black"){raw sbjct2}
    rectangle(15,75,120,60,:stroke=>"black",:fill=>rctng, :stroke_width=>2)
    text(75,115,"text-anchor"=>"middle", "dominant-baseline"=>"auto","font-family"=>"sans-serif","font-size"=>lbl2fs,:fill=>"black"){raw lbl2 + " " + i}
  end

  FileUtils.mkpath("site/images")
  file = "site/images/" + imgname + i.tr('.','') + ".svg"
  File.open(file, "w") { |f|
    img.write(f)
    f.write("<!-- Norbert.Reschke@gMail.com, https://creativecommons.org/licenses/by-nc-sa/4.0 -->")
    f.write("\n")
    f.write("<!-- made with rasem: https://github.com/aseldawy/rasem -->")
  }
}
