== Anforderungssituation 3 _Sicherung und Weiterentwicklung der Demokratie durch Partizipation_
Die Absolventinnen und Absolventen erschließen sich Möglichkeiten der politischen Mitwirkung und Mitgestaltung in unterschiedlichen gesellschaftlichen Zusammenhängen unter Beachtung demokratischer Grundprinzipien.

=== Zielformulierungen
Die Schülerinnen und Schüler diskutieren ihre _Chancen zur Mitgestaltung gesellschaftlicher Wirklichkeit auf unterschiedlichen Ebenen (Kommune, Land, Bund)_. Sie loten dabei _die Möglichkeiten parlamentarischer und außerparlamentarischer Mitwirkung im Gemeinwesen_ aus (ZF 1).

Sie erschließen verschiedene _politische Institutionen_ und erfassen ihre _Funktionen_ (ZF 2) und prüfen, inwieweit diese Institutionen ihren Interessen und Bedürfnissen gerecht werden (ZF 3).

Die Schülerinnen und Schüler erkennen Wahlen, wie z. B. die _Betriebsratswahl_ als Mittel und Möglichkeit der Einflussnahme (ZF 4).
