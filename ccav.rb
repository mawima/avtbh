#!/usr/bin/env ruby
# encoding: utf-8
require 'fileutils'
require 'rasem'
# https://github.com/aseldawy/rasem

fgr = '#000000'
bckgr = '#ffffff'
width = 150
height = 150

def cEye
  path(:stroke=>'#000000',:fill=>"none") {
    moveToA(50,51)
    arcToA(50,42,7,8,1,1,1)
  }
end

img = Rasem::SVGImage.new(:width=>width,:height=>height) {
  defs {
    group(:id => "head") {
      circle(63,58,35,:stroke=>"none",:fill=>"#ffffff") # white background for pupils head
      circle(63,58,27,:stroke=>fgr,:fill=>"none")
      cEye.translate(10,10)
      cEye.translate(30,10)
      line(70,31,72,26,:stroke=>fgr)#hair
      line(66,31,66,25,:stroke=>fgr)#hair
      line(50,29,52,33,:stroke=>fgr)#hair
      line(55,73,72,72,:stroke=>fgr)#mouth
    }
  }
  rectangle(0,0,width,height,:stroke=>"none",:fill=>"#ffffff",:opacity=>0.7)          #background transparent
  rectangle(0,0,130,13,:stroke=>"none",:fill=>"#efefef").translate(10,2)              #background Ausbildungsvorbereitung
  rectangle(0,0,12,105,:stroke=>"none",:fill=>"#efefef").translate(4,43)              #background HSBKOb
  rectangle(0,0,25,35,:stroke=>"none",:fill=>"#2189ff").translate(-5,75).rotate(-72)  #SP
  rectangle(0,0,20,30,:stroke=>"none",:fill=>"#ff7f2a").translate(5,25).rotate(-37)   #PO
  rectangle(0,0,30,55,:stroke=>"none",:fill=>"#e8b1ef").translate(135,2).rotate(35)
  rectangle(0,0,50,35,:stroke=>"none",:fill=>"#e8b16f").translate(10,100).rotate(18)  #BTHT
  rectangle(58,95,40,40,:stroke=>"none",:fill=>"#ffffff")# white background for pupils body
  rectangle(20,80,110,20,:stroke=>"none",:fill=>"#ffffff").rotate(-3)# white background for pupils arms
  rectangle(48,135,80,15,:stroke=>"none",:fill=>"#ffffff")# white background for pupils feet
  rectangle(0,0,30,35,:stroke=>"none",:fill=>"#b3ff80").translate(120,50).rotate(5)   #RE
  rectangle(0,0,30,75,:stroke=>"none",:fill=>"#fff000").translate(30,70).rotate(-28)  #MA
  rectangle(0,0,30,30,:stroke=>"none",:fill=>"#ff2525").translate(115,105).rotate(13) #EN
  use("head",:x=>20,:y=>0,:stroke_width=>2).scale(0.9,0.9).rotate(-3)
  line(26,95,68,95,:stroke=>fgr,:stroke_width=>2).rotate(-3) # left arm
  line(80,95,125,95,:stroke=>fgr,:stroke_width=>2).rotate(-3) # right arm
  line(75,78,75,84,:stroke=>fgr,:stroke_width=>2) # neck
  line(75,90,75,145,:stroke=>fgr,:stroke_width=>2) # body
  line(60,144,70,144,:stroke=>fgr,:stroke_width=>2) # left foot
  line(85,144,110,144,:stroke=>fgr,:stroke_width=>2) # right foot
  text(82,110,"font-family"=>"sans-serif",:fill=>fgr,"font-size"=>10){raw "BY"}
  text(82,120,"font-family"=>"sans-serif",:fill=>fgr,"font-size"=>10){raw "NC"}
  text(82,130,"font-family"=>"sans-serif",:fill=>fgr,"font-size"=>10){raw "SA"}
  text(0,0,"font-family"=>"sans-serif",:fill=>fgr,"font-size"=>8){raw "Hans-Sachs-Berufskolleg"}.translate(12,146).rotate(-90)
  text(75,12,"text-anchor"=>"middle","font-family"=>"sans-serif",:fill=>fgr,"font-size"=>10){raw "Ausbildungsvorbereitung"}
  text(0,0,"font-family"=>"sans-serif",:fill=>fgr,"font-size"=>4){raw "https://creativecommons.org/licenses/by-nc-sa/4.0"}.translate(30,90).rotate(-3) # arm
  text(0,0,"font-family"=>"sans-serif",:fill=>fgr,"font-size"=>4){raw "NorbertReschke@gMail.com"}.translate(width/2+5,height-5).rotate(-90)
  line(0,0,width,0,:stroke=>"#b3ff80",:stroke_width=>4)            # line top
  line(width,0,width,height,:stroke=>"#ff2525",:stroke_width=>4)   # line right
  line(0,height,width,height,:stroke=>"#ff7f2a",:stroke_width=>4)  # line bottom
  line(0,0,0,width,:stroke=>"#2189ff",:stroke_width=>4)            # line left
}

FileUtils.mkpath("site/images")
file = "site/images/" + "ccav.svg"

File.open(file,"w") { |f|
  img.write(f)
  f.write("<!-- Norbert.Reschke@gMail.com, https://creativecommons.org/licenses/by-nc-sa/4.0 -->")
  f.write("\n")
  f.write("<!-- made with rasem: https://github.com/aseldawy/rasem -->")
}
