== Anforderungssituation 4 _Funktionen_
Die Absolventinnen und Absolventen beschreiben angeleitet grundlegende Beziehungen und Veränderungen zwischen Größen. Sie agieren in schriftlicher und mündlicher Kommunikation situationsgerecht.

=== Zielformulierungen
Die Schülerinnen und Schüler kommunizieren:

Sie beschreiben einfache funktionale Zusammenhänge und ihre Darstellungen in Alltagssituationen (ZF 1).

Die Schülerinnen und Schüler verwenden mathematische Darstellungen:

Sie nutzen für funktionale Zusammenhänge unterschiedliche Darstellungsformen (ZF 2).

Die Schülerinnen und Schüler lösen Probleme mathematisch:

Sie unterscheiden proportionale und antiproportionale Zuordnungen in Sachzusammenhängen (ZF 3), stellen damit Berechnungen an und Beziehungen zwischen Größen dar (ZF 4).

Die Schülerinnen und Schüler modellieren:

Sie nutzen angeleitet die funktionalen Zusammenhänge im beruflichen Kontext (ZF 5) und stellen diese z.B. in Kleingruppen dar (ZF 6).
