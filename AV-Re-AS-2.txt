== Anforderungssituation 2 (ER) _Verständigung_
Die Absolventinnen und Absolventen kommunizieren einzelne Aspekte ihrer Einstellungen, Glaubens- und Lebenserfahrungen. Es gelingt ihnen in der Auseinandersetzung mit ausgewählten technischen und naturwissenschaftlichen Fragestellungen wesentliche religiöse Aspekte und daraus resultierende mögliche Konflikte vereinfacht zu skizzieren. Unter angemessener Verwendung einfacher religiöser, Begriffe gelingt in ausgewählten Teilbereichen, ein verantwortungsbewusst geführter Dialog.

Mögliche Anknüpfungspunkte zu beruflichen Handlungsfeldern:
HF 2 (Kundengerechte Information und Beratung); HF 1 (Personalmanagement; Informations- und Kommunikationsprozesse; Marketingstrategien und -aktivitäten; Präsentation von Produkten und Dienstleistungen)

Mögliche theologische Anknüpfungspunkte an die Handlungsfelder:
Christentum im Vergleich mit anderen Weltreligionen; Konfession und Ökumene; Glaube und Aberglaube; Wertvorstellungen; Idole und Statussymbole; Reichtum und Armut; Gerechtigkeit; religiöse Reflektion von Konsum

=== Zielformulierungen
Schülerinnen und Schüler beschreiben auf einfache Weise Gefühle, Lebens- und Glaubenserfahrungen und lassen sich auf Erfahrungen und Einstellungen anderer ein. (ZF 1)

Ausgehend von ihrer eigenen Konfession lernen sie einfache religiöse Begriffe unterschiedlicher Religionen und Konfessionen kennen und wenden diese im Austausch miteinander beispielhaft an (ZF 2).

Sie benennen aus ihren Alltagserfahrungen Verständigungsprobleme zwischen Religionen auch hinsichtlich naturwissenschaftlicher Vorstellungen und erarbeiten unter Anleitung gemeinsam Lösungen (ZF 3).

== Anforderungssituation 2 (KR)
Die Absolventinnen und Absolventen nehmen ausgewählte persönliche Erfahrungen von Glück und Unglück wahr. Sie benennen und akzeptieren unter Anleitung eigene Stärken und Schwächen. In Auseinandersetzung mit zentralen Aussagen der christlichen Heilszusage und in Kenntnis der eigenen Stärken und Schwächen entwickeln sie für sich kurz- und mittelfristig realisierbare Perspektiven für eine verantwortungsvolle Lebensgestaltung in überschaubaren Teilbereichen des privaten Umfeldes und hinsichtlich ihrer Berufsfindung.

== Zielformulierungen
Die Schülerinnen und Schüler beschreiben unter Anleitung ausgewählte Erfahrungen von Glück und Unglück (Besitz technischer Statussymbole). Sie tauschen sich über ihre mögliche Bedeutung für das Leben anhand einzelner Beispiele aus. (ZF 1)

Die Schülerinnen und Schüler schätzen zentrale Aussagen der christlichen Heilszusage (Auferstehung) als unabhängig von Leistung, gesellschaftlichem und persönlichem Ansehen grundlegend ein. Dabei setzen sie sich mit der Freiheit und den Möglichkeiten des Menschen auseinander, sich selbst zu entwickeln. Sie nehmen in der Auseinandersetzung mit diesen zentralen Aussagen ihre eigenen Stärken und Schwächen wahr und lernen, sie anzunehmen. (ZF 2)

Die Schülerinnen und Schüler erkennen, dass das Eröffnen der Wege zu Glück, Heil und Erlösung in unterschiedlichen Ausprägungen ein zentrales Anliegen aller Religionen ist. (ZF 3)

Schülerinnen und Schüler entwickeln in überschaubaren Teilbereichen Perspektiven für eine verantwortungsvolle Gestaltung ihres Lebens in Auseinandersetzung mit dem Wirken Jesu (z. B. Gleichnisse, Wunder- und Heilungsgeschichten, Bergpredigt) und dem heilsgeschichtlichen Ereignis von
Jesu Christi Tod und Auferstehung. (ZF 4)
