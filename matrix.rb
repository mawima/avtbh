#!/usr/bin/ruby
# encoding: utf-8
require 'fileutils'
require 'rasem'
# https://github.com/aseldawy/rasem

imgname = File.basename(__FILE__,".*")
fnts    = 20
white   = '#ffffff'
black   = '#000000'
red     = '#ff0000'
img = Rasem::SVGImage.new(:width => 150, :height => 150) do
  defs {
    group(:id => "arrow") {
      polygon([[0,0],[32,-10],[32,10]],:stroke=>"none",:fill=>red)
      line(32,0,50,0,:stroke=>red,:stroke_width=>4)
      }
    }

  rectangle(0,0,150,150,:stroke=>red,:fill=>white, :stroke_width=>4) # background

  use("arrow").translate(75+35,60).scale(-0.5,0.5)
  use("arrow").translate(75-35,60).scale(0.5,0.5)
  use("arrow").translate(75,60+35).scale(0.5,-0.5).rotate(90)
  use("arrow").translate(75,60-35).scale(0.5,0.5).rotate(90)

  text(75,125,"text-anchor"=>"middle", "dominant-baseline"=>"auto","font-family"=>"sans-serif","font-size"=>fnts,:fill=>red){raw imgname}

  end

  FileUtils.mkpath("site/images")
  file = "site/images/" + imgname + ".svg"

  File.open(file, "w") { |f|
    img.write(f)
    f.write("<!-- Norbert.Reschke@gMail.com, https://creativecommons.org/licenses/by-nc-sa/4.0 -->")
    f.write("\n")
    f.write("<!-- made with rasem: https://github.com/aseldawy/rasem -->")
    }

