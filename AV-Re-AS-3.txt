== Anforderungssituation 3 (ER) _Hermeneutik_
Ausgehend von einer konkreten Problemstellung lernen die Absolventinnen und Absolventen Grundzüge ausgewählter evangelischer Positionen kennen, wie sie sich aus der Auslegung von Texten ergeben. Dazu verwenden sie einfache Techniken zur Analyse religiöser Texte.

Fachlich-theologische Anknüpfungspunkte:

Bibelexegese; Texte, Texttypen und Traditionen; Symbole, Rituale und Feste; Leben und Tod; Trauer und Hoffnung

=== Zielformulierungen
Die Schülerinnen und Schüler lernen zentrale Aussagen ausgewählter biblischer Texte und religiöser Zeugnisse kennen. Sie beschreiben den dazugehörigen historischen Kontext (ZF 1).

Die Schülerinnen und Schüler wenden bei ihrer gemeinsamen Arbeit in der Gruppe einfache Analysetechniken und darstellende Methoden (ZF 2). Sie erkennen ausgewählte religiöse Zeugnisse als Angebot zur Lebensorientierung und tauschen sich unter Anleitung darüber aus (ZF 3).

== Anforderungssituation 3 (KR)
In der Auseinandersetzung mit aktuellen ökologischen Problemlagen, die im beruflichen und persönlichen Umfeld erfahrbar werden, erfassen die Absolventinnen und Absolventen die Bedrohung der Schöpfung. Anhand ausgewählter biblischer Schöpfungserzählungen deuten sie den Herrschaftsauftrag des Menschen und entwickeln daraus beispielhaft konkrete Perspektiven für einen verantwortungsvolleren Umgang mit der Schöpfung.

=== Zielformulierungen
Die Schülerinnen und Schüler verstehen anhand der biblischen Schöpfungserzählungen, dass es sich bei ihnen weder um historische Berichte noch um naturwissenschaftliche Modelle handelt. Sie deuten diese unter Anleitung als Darstellung eines dauerhaften Beziehungsgeschehens zwischen Gott, Mensch und Welt. (ZF 1)

Die Schülerinnen und Schüler nehmen die Aufträge "`Herrschen, Bebauen und Bewahren`" als Grundauftrag Gottes an die Menschen wahr. Sie erkennen, dass Technik Natur immer verändert und bewerten an ausgewählten Beispielen die Folgen. Geleitet von dem vorgegebenen Herrschaftsauftrag ermitteln sie die besondere und persönliche Verantwortung jedes Menschen für die Schöpfung, die sie an Beispielen aus dem beruflichen und dem privaten Handelns veranschaulichen. (ZF 2)
