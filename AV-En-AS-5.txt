== Anforderungssituation 5 _Inner- und außerbetriebliche Kommunikation_
Absolventinnen und Absolventen bearbeiten angeleitet ausgewählte Aspekte der inner- und außerbe-
trieblichen Kommunikation.

=== Zielformulierungen (GER – Niveaustufe A2/DQR)
Schülerinnen und Schüler verstehen _aufbereitete Mitteilungen_ (z. B. Besprechungsunterlagen/ Konferenzprotokolle, Tagesordnungen) _und Informationen_ (z. B. über Materialeigenschaften, Bauelemente, technische Zeichnungen, Montagepläne, Schaltpläne, herstellerspezifische Service- und Wartungspläne, Checklisten, Reparaturmaßnahmen) in überschaubaren Teilbereichen. (Rezeption mündlich
und schriftlich) (ZF 1)

Schülerinnen und Schüler erstellen unter Anleitung einfache, aufbereitete _Mitteilungen für Besprechungen und Konferenzen_ (z. B. Nachrichten auf einem Anrufbeantworter, Kurzmitteilungen, Termin- und Reiseplanungen). (Produktion mündlich und schriftlich) (ZF 2)

Schülerinnen und Schüler stellen _einfache Unterlagen für Besprechungen_ zusammen (z. B. Berichte, Statistiken, Diagramme zu Materialeigenschaften, Bauelemente, technische Zeichnungen, Montagepläne, Schaltpläne, herstellerspezifische Service- und Wartungspläne, Checklisten, Reparaturmaßnahmen). (Produktion schriftlich) (ZF 3)

Schülerinnen und Schüler führen auf wesentliche Details beschränkte _berufliche und persönliche Gespräche_ (z. B. Telefonate, Small Talk, Beiträge in Besprechungen und Konferenzen) unter Berücksichtigung landestypischer Kommunikations- und Höflichkeitsregeln. (Interaktion mündlich)
(ZF 4)

Schülerinnen und Schüler tauschen sich schriftlich unter Berücksichtigung landestypischer Kommunikations- und Höflichkeitsregeln in begrenztem Rahmen über vorstrukturierte typische Inhalte des Handwerks und der Industrie aus (z. B. über Materialeigenschaften, Bauelemente, technische Zeichnungen, Montagepläne, Schaltpläne, herstellerspezifische Service- und Wartungspläne, Checklisten, Reparaturmaßnahmen, Liefertermine). (Interaktion schriftlich) (ZF 5)
