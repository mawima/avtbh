#!/usr/bin/env ruby
# encoding: utf-8
require 'fileutils'
require 'rasem'
# https://hexapdf.gettalong.org
include Math

imgname = File.basename(__FILE__,".*")
white  = '#ffffff'
black  = '#000000'
grey   = '#bccdf7'
green  = '#49db44'
tbh    = '#e8b16f'

width  = 150
height = 150
radius = 70
center = 75

def pie(c,r,i,s) # new (and hopefully better to understand and read) version of quarter (a241.rb) 
  a = 9 *i.to_i
  if a <= 180
    t = 0  # if angle 0 to 180 degrees
  elsif a > 180
    t = 1  # if angle > 180 degrees, with arcTo only makes sense if not 360 degrees
  end
        
  if a < 360
    b = 1
  elsif a == 360
    a = 359.99 # ugly workaround to draw something that looks like a circle
  elsif a > 360
    puts "something went totally wrong :("
    puts "your circle has more than 360°"
    exit  
  end
  
  path(:stroke=>s,:fill=>c,:opacity=>1.0) {
    moveToA(0,0)
    lineToA(0,-r) # last point before starting segment
    arcToA(r*sin(a*PI/180),-r*cos(a*PI/180),r,r,0,t,1) # (1) from last point go to <x>, (2) from last point go to <y>, (3) radius <x>, (4) radius <y> (circle x=y), (5) set to 0 or read docs, (6) 0 if <= 180 degrees,1 if > 180 degrees , (7) clockwise direction = 1, counterclockwise direction = 0
    close
  } 
  
  
end

def leaf(s,c,o) # week pointer
  rectangle(-5.3,-74,10.6,47,:stroke=>s,:fill=>c,:opacity=>o)
end 

(1..40).each { |i|
  img = Rasem::SVGImage.new(:width => 150, :height => 150) do
    rectangle(0,0,150,150,:stroke=>"none",:fill=>white)                   # background
    text(0,0,"text-anchor"=>"left","font-family"=>"sans-serif",:fill=>grey,"font-size"=>8){raw "Schulwochen"}.translate(1,149)
#    text(0,0,"text-anchor"=>"middle","font-family"=>"sans-serif",:fill=>black,"font-size"=>4){raw "https://creativecommons.org/licenses/by-nc-sa/4.0 - HSBK-Oberhausen"}.translate(center,149)
    circle(75,75,radius,:stroke=>"none",:fill=>grey,:opacity=>0.5)        # circle quarter pies
    pie(tbh,radius,i,"none").translate(center,center)                     # last to present weeks
    circle(75,75,radius,:stroke=>black,:fill=>"none")                     # outline for circle quarter pies
    circle(75,75,radius-35,:stroke=>black,:fill=>white)                   # center circle as basis for leaf and present weeknumber
    line(0,-31,0,-74,:stroke=>black).translate(center,center)             # 00:00 quarterline
    line(0,-31,0,-74,:stroke=>black).translate(center,center).rotate(90)  # 03:00 quarterline
    line(0,-31,0,-71,:stroke=>black).translate(center,center).scale(1,-1) # 06:00 quarterline
    line(0,-31,0,-74,:stroke=>black).translate(center,center).rotate(-90) # 09:00 quarterline
    leaf(black,green,1.0).translate(center,center).rotate(9*i.to_i-4.5)   # week pointer    
    (1..i).each { |j|
      text((radius-7)*sin((j*9-4.5)*PI/180)+75,78-(radius-7)*cos((j*9-4.5)*PI/180), "text-anchor"=>"middle","font-family"=>"sans-serif","font-size"=>7,:fill=>black){raw j}
    } # weeknumbers
    text(0,0,"text-anchor"=>"middle","font-family"=>"sans-serif",:fill=>black,"font-size"=>4){raw "Hans-Sachs"}.translate(center,center-21)
    text(0,0,"text-anchor"=>"middle","font-family"=>"sans-serif",:fill=>black,"font-size"=>4){raw "Berufskolleg"}.translate(center,center-17)
    text(center,center+15, "text-anchor"=>"middle","font-family"=>"sans-serif","font-size"=>30,:fill=>black){raw i}       # present weeknumber
    text(center,center-9, "text-anchor"=>"middle","font-family"=>"sans-serif","font-size"=>10,:fill=>black){raw "Woche"} # label for present weeknumber
    text(0,0,"text-anchor"=>"middle","font-family"=>"sans-serif",:fill=>black,"font-size"=>3){raw "Ausbildungsvorbereitung"}.translate(center,center+19)
  end

  FileUtils.mkpath("site/images")
  file = "site/images/" + imgname + "%02d" % + i.to_s + ".svg"

  File.open(file, "w") { |f|
    img.write(f)
    f.write("<!-- Norbert.Reschke@gMail.com, https://creativecommons.org/licenses/by-nc-sa/4.0 -->")
    f.write("\n")
    f.write("<!-- made with rasem: https://github.com/aseldawy/rasem -->")
  }
}
