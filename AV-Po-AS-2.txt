== Anforderungssituation 2 _Berufsorientierung und Arbeitsplatzsicherung im Wirtschaftssystem_
Die Absolventinnen und Absolventen erschließen unter Anleitung grundlegende Anforderungsprofile in ausgewählten technischen Berufsfeldern. Sie überprüfen anhand eigener und fremder Erfahrungen ihren Berufswunsch bzw. ihre bereits getroffene Berufsentscheidung.

=== Zielformulierungen
Die Schülerinnen und Schüler ermitteln unter Anleitung ihre eigenen Stärken sowie Fähigkeiten und setzen sich mit ihrer _Berufswahl_ und möglichen Perspektiven auseinander (ZF 1).

Im Rahmen von beruflichen Handlungssituationen erkennen sie benötigte _Schlüsselqualifikationen_ in unterschiedlichen technischen Arbeitszusammenhängen (ZF 2).

Die Schülerinnen und Schüler entwickeln unter Anleitung _individuelle Strategien der Arbeitsplatzsicherung_ (ZF 3).
